# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Field, Item


class ProductItem(Item):
    name = Field()
    price = Field()
    rating = Field()
    store = Field()
    url_in_store = Field()
    categories = Field()
    features = Field()
    images = Field()
    description = Field()
    reviews = Field()
