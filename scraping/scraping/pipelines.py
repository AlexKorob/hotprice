# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
from spiders.tasks import save_products
from spiders.models import Spider


class ScrapingPipeline:
    def __init__(self):
        self.limit_to_save = 30
        self.items = []

    def process_item(self, item, spider):
        # print(item)
        item = dict(item)
        if item["name"]:
            self.items.append(dict(item))
        if len(self.items) == self.limit_to_save or spider.worked:
            save_products.delay(self.items)
            self.items = []

    def close_spider(self, spider):
        print(spider.name)
        Spider.objects.filter(name=spider.name).update(jobid="", activate=False)
        spider.logger.info('Spider closed: %s', spider.name)

    # def open_spider(self, spider):
    #     self.file = open('name_items_from_neon.txt', 'w')
    #     self.file.write('[')
    #
    # def process_item(self, item, spider):
    #     line = json.dumps(dict(item), ensure_ascii=False) + ",\n"
    #     self.file.write(line)
    #     return item
    #
    # def close_spider(self, spider):
    #     self.file.write(']')
    #     self.file.close()
