import re
from string import ascii_letters
from scrapy import Request, Spider
from django.conf import settings
from scrapy_redis.spiders import RedisSpider
from ..items import ProductItem
from categories.models import Category
from .spider_extend import SpiderExtend


class XcomSpider(SpiderExtend, Spider):
    name = "xcom"
    allowed_domains = ("xcom.ua", )
    # start_urls = ("https://xcom.ua", )
    prefix_url = "https://xcom.ua"

    def parse(self, response):
        categories = self.get_categ(response)
        list_for_parse = self.get_for_parse(categories)
        for item in list_for_parse: # change this for config spider crawling
            url = self.prefix_url + item["url"]
            yield Request(url, callback=self.parse_pages, meta=item)

    def parse_pages(self, response):
        urls = response.xpath('//div[@class="item"]/div[@class="title"]/a[position()=last()]/@href').extract()
        for url in urls[:12]: # change this for config spider crawling
            meta = response.meta
            meta["url_in_store"] = self.prefix_url + str(url)
            yield Request(self.prefix_url + url, callback=self.parse_detail, meta=meta)
        next_page = response.xpath('//div[@class="pages"]/span[@class="currentpage"]/following-sibling::\
                                   a[position()=1]/@href').extract_first()
        # if next_page:
        #     yield Request(self.prefix_url + next_page, callback=self.parse_pages)

    def parse_detail(self, response):
        fields_item = ProductItem()
        fields_item["name"] = response.xpath('//div[@itemprop="name"]/h1/text()').extract_first().strip()
        fields_item["price"] = self.get_price(response)
        fields_item["rating"] = self.get_rating(response)
        fields_item["store"] = self.name
        fields_item["url_in_store"] = response.meta["url_in_store"]
        fields_item["categories"] = self.get_categories(response)
        fields_item["features"] = self.get_features(response)
        fields_item["images"] = self.get_images(response)
        fields_item["description"] = self.get_description(response)
        fields_item["reviews"] = self.get_reviews(response)

        return fields_item

    def get_price(self, response):
        price = response.xpath('//div[@class="price1"]/div[@itemprop="price"]/text()').extract_first()
        return re.sub(r'[^0-9.]+', r'', price)

    def get_categories(self, response):
        categories = [categ.strip() for categ in response.xpath('//div[@itemprop="breadcrumb"]/span/a/span/text()')\
                      .extract()]
        last_category = categories[-1].strip()
        last_category_is_eng_alpha = True
        ascii_chars = ascii_letters + " .',-"
        for char in last_category:
            if char not in ascii_chars:
                last_category_is_eng_alpha = False
                break
        if last_category.isupper() and last_category_is_eng_alpha and len(categories) > 1:
            return categories[:-1]
        return categories

    def get_reviews(self, response):
        reviews = []
        reviews_on_site = response.xpath('//table[@class="comments"]//tr')
        if not reviews_on_site:
            return reviews

        for row in reviews_on_site:
            name = row.xpath('.//th/text()').extract_first()
            if name:
                review = {}
                review["username"] = name
                review["date"] = row.xpath('.//td/text()').extract_first().strip()
                advantages = row.xpath('.//following-sibling::tr/td/text()').extract_first().strip()
                if advantages == "Достоинства:":
                    review["advantages"] = row.xpath('.//following-sibling::tr/td[position()=2]/text()').\
                                                     extract_first()
                if advantages == "Недостатки:":
                    review["advantages"] = row.xpath('.//following-sibling::tr/td[position()=2]/text()').\
                                                     extract_first()
                disadvantages = row.xpath('.//following-sibling::tr/following-sibling::tr/td/text()').\
                                          extract_first().strip()
                if disadvantages == "Недостатки:":
                    review["disadvantages"] = row.xpath('.//following-sibling::tr/following-sibling::tr/\
                                                        td[position()=2]/text()').extract_first().strip()
                text = row.xpath('.//following-sibling::tr/td[@class="comment"]/text()') or \
                       row.xpath('.//following-sibling::tr/following=sibling::tr/td[@class="comment" ]/text()') or \
                       row.xpath('.//following-sibling::tr/following-sibling::tr/following-sibling::tr/\
                                 td[@class="comment"]/text()')
                review["text"] = text.extract_first().strip()
                reviews.append(review)

        return reviews

    def get_description(self, response):
        description_text = response.xpath('//div[@id="article"]/p/text()').extract_first()
        if not description_text:
            description_text = "<br>".join(["".join(arr.split("\n")) \
                                           for arr in response.xpath('//div[@id="article"]/text()').extract()])
        return description_text

    def get_images(self, response):
        images = []
        front_img = response.xpath('//a[@class="gallery"]/@href').extract_first()
        if front_img:
            images.append(self.prefix_url + front_img)
            images.extend([self.prefix_url + url_img for url_img in response.\
                           xpath('//div[@class="gallery"]/div/a/@href').extract()])
        return images

    def get_features(self, response):
        keys = response.xpath('//div[@id="desc"]/table//tr/th/span/text()').extract()
        values = response.xpath('//div[@id="desc"]/table//tr/td/text()').extract()
        features = {}
        for key, value in zip(keys, values):
            features[key] = value
        return features

    def get_rating(self, response):
        rating = response.xpath('//div[@class="stars"]/div[@class="star_full"]')
        if rating:
            return len(rating)
        return 0

    def get_for_parse(self, categories):
        categories_for_parse = []
        def recursive(categories):
            for key, value in categories.items():
                if isinstance(value, dict):
                    recursive(value)
                elif isinstance(value, list):
                    for val in value:
                        val["parent_category_on_db"] = key
                        categories_for_parse.append(val)
        recursive(categories)
        return categories_for_parse

    def get_categ(self, response):
        categories = {}
        top_category_level = response.xpath('//table[@id="topmenuitems"]//td[@class="topmenuelement"]/text()').\
                             extract()

        for i in range(len(top_category_level)):
            sub_categories_selectors = response.xpath('//div[@class="innermenu"]')[i].xpath('.//td')
            sub_categories = {}
            for sub_cat in sub_categories_selectors:
                sub_cat_level_2 = sub_cat.xpath('.//a[1]/text()').extract_first()
                xpath_request = f'.//a[text() != "показать все" and text() \
                                != "{sub_cat_level_2}" and text() != "свернуть"]'
                sub_cat_level_3 = []
                for cat in sub_cat.xpath(xpath_request):
                    temp_dict = {}
                    category_name = cat.xpath('.//text()').extract_first()
                    category_url = cat.xpath('.//@href').extract_first()
                    if category_name and category_url:
                        temp_dict["category"] = category_name
                        temp_dict["url"] = category_url
                        sub_cat_level_3.append(temp_dict)

                sub_categories[sub_cat_level_2] = sub_cat_level_3
                categories[top_category_level[i]] = sub_categories
        return categories

    def find_parents_on_dict(self, d, value):
        for k, v in d.items():
            if isinstance(v, dict):
                p = self.find_parents_on_dict(v, value)
                if p:
                    return [k] + p
            elif k == value:
                return [k]

    # def save_cat_to_db(self, dictionary):
    #     for key,value in dictionary.items():
    #         if key is not None:
    #             parents = self.find_parents_on_dict(self.categories, key)
    #             if parents:
    #                 parent = Category.objects.get(name=parents[-2])
    #                 Category.objects.get_or_create(name=key, parent=parent)
    #                 print(parents[-2], key)
    #             else:
    #                 print(key)
    #                 Category.objects.get_or_create(name=key)
    #
    #         if isinstance(value, dict):
    #             print(key)
    #             self.save_cat_to_db(value)
    #         elif isinstance(value, list):
    #             if key is not None:
    #                 for name in value:
    #                     print(key)
    #                     parent = Category.objects.get(name=key)
    #                     print(parent, name)
    #                     Category.objects.get_or_create(name=name, parent=parent)
