import re
from scrapy import Spider, Request
from ..items import ProductItem
from scrapy_redis.spiders import RedisSpider
from .spider_extend import SpiderExtend
from scrapy.exceptions import CloseSpider


class NeonSpider(SpiderExtend, Spider):
    name = "neon"
    prefix_url = "https://neon.ua"

    def parse(self, response):
        level_1_categories_selectors = response.xpath('.//ul[@id="topmenuitems"]/li[@class="topmenuelement"]/a')
        for level_1_category_selector in level_1_categories_selectors: # change this for config spider crawling
            level_1 = level_1_category_selector.xpath('.//text()').extract_first()
            level_1_category_url = level_1_category_selector.xpath('.//@href').extract_first()
            yield Request(self.prefix_url + level_1_category_url, callback=self.parse_category, meta={"level_1": level_1})

    def parse_category(self, response):
        selectors = response.xpath('.//div[contains(@class, "subList") and position()>1]')
        url_list = []
        for selector in selectors:
            sub_selectors = selector.xpath('.//div[contains(@class, "subList")]')
            if sub_selectors:
                for sub_selector in sub_selectors:
                    url_list.extend(sub_selector.xpath('.//div[contains(@class, "subCat")]/a/@href').extract())
                continue
            url_list.extend(selector.xpath('.//div[contains(@class, "subCat")]/a/@href').extract())
        for url in url_list: # change this for config spider crawling
            yield Request(self.prefix_url + url, callback=self.parse_pages, meta={"url": url})

    def parse_pages(self, response):
        for url in response.xpath('.//div[@class="item"]/div[@class="title"]/a/@href').extract()[:12]: # # change this for config spider crawling
            meta = response.meta
            meta["url_in_store"] = self.prefix_url + str(url)
            yield Request(self.prefix_url + url, callback=self.parse_detail, meta=meta)

        next_page = response.xpath('//div[@class="pages"]/span[@class="currentpage"]/following-sibling::\
                                   a[position()=1]/@href').extract_first()
        # if next_page:
        #     yield Request(self.prefix_url + next_page, callback=self.parse_pages)

    def parse_detail(self, response):
        fields_items = ProductItem()
        fields_items["name"] = response.xpath('//div[@id="main"]/h1/text()').extract_first().strip()
        fields_items["price"] = self.get_price(response)
        fields_items["rating"] = self.get_rating(response)
        fields_items["store"] = self.name
        fields_items["url_in_store"] = response.meta["url_in_store"]
        fields_items["categories"] = self.get_categories(response)
        fields_items["features"] = self.get_features(response)
        fields_items["images"] = self.get_images(response)
        fields_items["description"] = self.get_description(response)
        fields_items["reviews"] = self.get_reviews(response)

        return fields_items

    def get_price(self, response):
        price = response.xpath('//div[@class="price-2"]/text()').extract_first().strip()
        return re.sub(r'[^0-9.]+', r'', price)

    def get_description(self, response):
        description_exists = response.xpath('//div[@id="article"]/*[not(self::h2)]')
        if description_exists:
            description = ''.join([text.strip() for text in response.\
                                  xpath('//div[@id="article"]/node()[not(self::h2)]').extract()]).replace('\xa0', ' ')
            return description
        return ""

    def get_images(self, response):
        front_img = response.xpath('//a[@class="gallery"]/@href').extract_first()
        gallery = response.xpath('//div[@class="gallery"]/a/@href').extract()
        images = []
        if front_img:
            images.append(self.prefix_url + front_img)
        if gallery:
            for img_url in gallery:
                images.append(self.prefix_url + img_url)
        return images

    def get_rating(self, response):
        rating = response.xpath('//div[@class="stars"]/div[@class="star_full"]')
        if rating:
            return len(rating)
        return 0

    def get_categories(self, response):
        categories = [categ.strip() for categ in response.xpath('//div[@id="path"]/a[position()>1]/text()')\
                      .extract()]
        return categories

    def get_features(self, response):
        features_table_selectors = response.xpath('//div[@id="desc"]/table//tr')
        features = {}
        if features_table_selectors:
            for select in features_table_selectors:
                features[select.xpath('./td/font/text()').extract_first()] = \
                         select.xpath('./td[position()=last()]/text()').extract_first().strip()
            return features
        return {}

    def get_reviews(self, response):
        review = response.xpath('//div[@id="review"]/*[not(self::h2)]').extract()
        if review:
            raise CloseSpider(f"I has found One review on neon: {response.meta['url_in_store']}")
        return []
