from abc import ABC, abstractmethod, abstractproperty
from functools import wraps
from scrapy.exceptions import CloseSpider
from spiders.models import Spider
from scrapy import Request


class SpiderABC(ABC):

    @abstractproperty
    def name(self) -> str:
        pass

    @abstractmethod
    def parse_pages(self):
        pass

    @abstractmethod
    def get_price(self) -> str:
        pass

    @abstractmethod
    def get_description(self) -> str:
        pass

    @abstractmethod
    def get_rating(self) -> int:
        pass

    @abstractmethod
    def get_images(self) -> list:
        """
            return: list with urls
        """

    @abstractmethod
    def get_categories(self) -> list:
        """
            Product category must be in the end of list
        """

    @abstractmethod
    def get_features(self) -> dict:
        """
            KEYS: - feature
                  - value
        """

    @abstractmethod
    def get_reviews(self) -> list:
        """
            list contain dicts
            [
                KEYS: - username
                      - text
                      - advantages
                      - disadvantages
                      - date
            ]
        """


def strict_type(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        name_func_in_subclass = func.__name__
        type_of_return = getattr(SpiderABC, func.__name__).__annotations__.get("return")
        if type_of_return:
            returned = func(*args, **kwargs)
            if isinstance(returned, type_of_return):
                return returned
            raise CloseSpider(f"{func.__name__} returned {type(returned)}, but must be: {type_of_return}")
        return func(*args, **kwargs)
    return wrapper


class SpiderExtend(SpiderABC):
    start_urls = []
    pages_to_parse = []

    def __init__(self, *args, **kwargs):
        for cls in SpiderExtend.__subclasses__():
            for attr in cls.__dict__:
                if callable(getattr(cls, attr)) and hasattr(SpiderABC, attr):
                    setattr(cls, attr, strict_type(getattr(cls, attr)))
        super().__init__(*args, **kwargs)

        if not (kwargs.get("urls")):
            self.start_urls.append(Spider.objects.get(name=self.name).start_url)
        else:
            self.pages_to_parse.extend([url.strip() for url in kwargs.get("urls").split(";") if url.strip() != ""])

    def start_requests(self):
        if self.start_urls:
            for url in self.start_urls:
                yield Request(url, callback=self.parse)
        else:
            for url in self.pages_to_parse:
                yield Request(url, callback=self.parse_pages)

    @property
    def worked(self):
        inprogress = len(self.crawler.engine.slot.inprogress)
        if inprogress <= 1:
            return True
        return False
