import re
from scrapy import Spider, Request
from ..items import ProductItem
from scrapy_redis.spiders import RedisSpider
from .spider_extend import SpiderExtend
from scrapy.exceptions import CloseSpider


class MyBaySpider(SpiderExtend, Spider):
    name = "mybay"

    def parse(self, response):
        categories_urls = response.xpath('//div[@class="dropdown-menu"]/div[@class="dropdown-inner"]/ul/li/a/@href')\
                          .extract()

        for category_url in categories_urls:
            yield Request(category_url, callback=self.parse_pages)

    def parse_pages(self, response):
        extend_categories_urls = response.xpath('//a[@class="category_name"]/@href').extract()
        if extend_categories_urls:
            for url in extend_categories_urls:
                yield Request(url, callback=self.parse_pages)
        else:
            # last_page = [int(i) for i in reversed(t.split()) if i.isdigit()][0] # change this for config spider crawling
            last_page = 1

            for i in range(1, int(last_page) + 1):
                yield Request(response.url + f"?page={i}", callback=self.parse_products_url_from_page)

    def parse_products_url_from_page(self, response):
        urls = response.xpath('//h4/a/@href').extract()
        for url in urls: # change this for config spider crawling
            yield Request(url, callback=self.parse_detail, meta={"url_in_store": url})

    def parse_detail(self, response):
        fields_items = ProductItem()
        fields_items["name"] = response.xpath('//div[@class="col-sm-12"]/h1/text()').extract_first().strip()
        fields_items["price"] = self.get_price(response)
        fields_items["rating"] = self.get_rating(response)
        fields_items["store"] = self.name
        fields_items["url_in_store"] = response.meta["url_in_store"]
        fields_items["categories"] = self.get_categories(response)
        fields_items["features"] = self.get_features(response)
        fields_items["images"] = self.get_images(response)
        fields_items["description"] = self.get_description(response)
        fields_items["reviews"] = self.get_reviews(response)

        return fields_items

    def get_price(self, response):
        price = response.xpath('//ul[@class="list-unstyled"]/li/h2[@class="page-product"]/text()').\
                extract_first() or response.xpath('//ul[@class="list-unstyled"]/li/h2[@class="page-product1"]/text()').\
                extract_first()
        price = re.sub(r'[^0-9.]+', r'', price.strip())
        if price[-1] == ".":
            price = price[:-1]
        return price

    def get_rating(self, response):
        rating = len(response.xpath('//span[@class="fa fa-stack"]/i[@class="fa fa-star fa-stack-1x"]').extract())
        return rating

    def get_categories(self, response):
        categories = [categ.strip() for categ in response.xpath('//ul[@class="breadcrumb"]/li/a/text()')\
                      .extract()[1:-1]]
        return categories

    def get_features(self, response):
        features_and_values = zip(response.xpath('//div[@id="tab-description"]//table//tr[count(td)=2]/td[position()=1]/text()').extract(),
                                  response.xpath('//div[@id="tab-description"]//table//tr[count(td)=2]/td[position()=2]/text()').extract())
        features = {}
        for feature, value in features_and_values:
            features[feature.strip()] = value.strip()
        return features

    def get_images(self, response):
        images_from_site = response.xpath('//ul[@class="thumbnails"]/li/a/@href').extract()
        images = []
        for image in images_from_site:
            images.append(image)
        return images

    def get_description(self, response):
        description = response.xpath('//div[@class="tab-pane active"]/p/text()').extract_first()
        if description and len(description) > 8:
            return description
        return ""

    def get_reviews(self, response):
        return []
