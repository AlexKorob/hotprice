from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from .views import FacebookLogin, TwitterLogin, FacebookConnect, TwitterConnect
from rest_auth.registration.views import SocialAccountListView, SocialAccountDisconnectView


urlpatterns = [
    path('token-auth/', obtain_auth_token),
    path('auth/', include('rest_auth.urls')),
    path('registration/', include('rest_auth.registration.urls')),

    path('auth/facebook/', FacebookLogin.as_view(), name='facebook_login'),
    path('auth/twitter/', TwitterLogin.as_view(), name='twitter_login'),
    path('auth/facebook/connect/', FacebookConnect.as_view(), name='facebook_connect'),
    path('auth/twitter/connect/', TwitterConnect.as_view(), name='twitter_connect'),
    path('socialaccounts/', SocialAccountListView.as_view(), name='social_account_list'),
    path('socialaccounts/<int:pk>/disconnect/', SocialAccountDisconnectView.as_view(), name='social_account_disconnect'),
]
