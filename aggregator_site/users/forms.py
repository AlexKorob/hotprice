from django.contrib.auth.forms import UserCreationForm
from .models import User
from django.forms import ValidationError


class UserCreateForm(UserCreationForm):
    UserCreationForm.error_messages = {
        'password_mismatch': "Passwords not match",
    }

    class Meta:
        model = User
        fields = ["username", "first_name", "last_name", "email"]

    def clean_username(self):
        username = self.cleaned_data["username"]
        if User.objects.filter(username=username):
            error_message = "This login does exists"
            raise ValidationError(error_message)
        elif len(username) <= 2:
            error_message = "Login must contains more than 3 symbols"
            raise ValidationError(error_message)
        return username

    def clean_first_name(self):
        if len(self.cleaned_data["first_name"]) <= 2:
            error_message = "Name must contains more than 3 symbols"
            raise ValidationError(error_message)
        return self.cleaned_data['first_name']

    def clean_last_name(self):
        if len(self.cleaned_data["last_name"]) <= 2:
            error_message = "Surname must contains more than 3 symbols"
            raise ValidationError(error_message)
        return self.cleaned_data['last_name']
