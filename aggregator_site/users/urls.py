from django.urls import path
from . import views

app_name = "users"

urlpatterns = [
    path("sign-in/", views.UserAuthentication.as_view(), name="sign_in"),
    path("sign-out/", views.UserLogout.as_view(), name="sign_out"),
    path("registration/", views.UserCreate.as_view(), name="registration"),
    path("verify/<str:uuid>/", views.VerifyEmail.as_view(), name="verify_email"),
]
