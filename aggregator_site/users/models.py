import uuid
import logging
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from django.urls import reverse
from django.db.models import signals
from django.db.models.signals import post_save
from rest_framework.authtoken.models import Token
from django.utils.translation import gettext_lazy as _
from django.core.mail import send_mail


log = logging.getLogger("file")


class User(AbstractUser):
    email = models.EmailField(_('email address'), unique=True, blank=False)
    is_verified = models.BooleanField(default=False)
    verification_uuid = models.UUIDField('Unique Verification UUID', default=uuid.uuid4)
    sended_verification_on_email = models.BooleanField(default=False)


@receiver(post_save, sender=User)
def send_verify_message(sender, instance, signal, created=False, *args, **kwargs):
    if not instance.is_verified and not instance.sended_verification_on_email:
        # WARNING domain is hard coded
        domain = "https://hotprice.cf"
        url = domain + reverse("users:verify_email", args=(str(instance.verification_uuid), ))
        send_mail(
            'Verify your account',
            f"Follow this link to verify your account: {url}",
            'from@hotprice.com',
            [instance.email],
            fail_silently=False)
        User.objects.filter(id=instance.id).update(sended_verification_on_email=True)
        log.info(f"For user - {instance.username} received verification email")


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


# signals.post_save.connect(send_verify_message, sender=User)
# signals.post_save.connect(create_auth_token, sender=User)
