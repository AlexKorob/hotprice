import logging
from django.shortcuts import reverse
from django.views.generic import View
from .models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.http import HttpResponsePermanentRedirect, HttpResponse, Http404
from .forms import UserCreateForm
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from allauth.socialaccount.providers.twitter.views import TwitterOAuthAdapter
from rest_auth.registration.views import SocialLoginView, SocialLoginView
from rest_auth.social_serializers import TwitterLoginSerializer, TwitterConnectSerializer
from rest_auth.registration.views import SocialConnectView


log = logging.getLogger("file")


class UserLogout(View):
    def get(self, request):
        logout(request)
        log.info(f"User - {request.user.username} logout")
        return HttpResponsePermanentRedirect(reverse("products:index"))


class VerifyEmail(View):
    def get(self, request, uuid):
        try:
            user = User.objects.get(verification_uuid=uuid, is_verified=False)
        except User.DoesNotExist as e:
            log.error(e)
            raise Http404("User does not exist or is already verified")

        user.is_verified = True
        user.save()
        log.info(f"User - {user.username} was verified")
        return HttpResponsePermanentRedirect(reverse("products:index"))


class UserAuthentication(View):
    def post(self, request):
        bound_form = AuthenticationForm(data=request.POST)
        if bound_form.is_valid():
            user = authenticate(username=bound_form.cleaned_data["username"],
                                password=bound_form.cleaned_data["password"])
            login(request, user)
            log.info(f"User - {user.username} authenticated")
            return HttpResponse("OK")
        error = list(bound_form.errors.values())[0]
        return HttpResponse(error)


class UserCreate(View):
    def post(self, request):
        bound_form = UserCreateForm(request.POST)

        if bound_form.is_valid():
            bound_form.save()
            user = authenticate(username=bound_form.cleaned_data["username"],
                                password=bound_form.cleaned_data["password2"])
            log.info(f"User - {user.username} created")
            login(request, user)
            return HttpResponse("OK")
        return HttpResponse(bound_form.errors.as_json())


class FacebookLogin(SocialLoginView):
    adapter_class = FacebookOAuth2Adapter


class TwitterLogin(SocialLoginView):
    serializer_class = TwitterLoginSerializer
    adapter_class = TwitterOAuthAdapter


class FacebookConnect(SocialConnectView):
    adapter_class = FacebookOAuth2Adapter


class TwitterConnect(SocialConnectView):
    serializer_class = TwitterConnectSerializer
    adapter_class = TwitterOAuthAdapter
