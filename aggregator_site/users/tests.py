import re
from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.forms import AuthenticationForm
from django.core import mail
from .models import User


class UserCreateTestCase(TestCase):

    def setUp(self):
        self.data = {"username": "Nikola",
                     "email": "mkv-k@i.ua",
                     "first_name": "Nikola",
                     "last_name": "Tesla",
                     "password1": "1tunguska_meteorite1",
                     "password2": "1tunguska_meteorite1"}

    def test_user_created_and_verify_email(self):
        client = self.client.post(reverse("users:registration"), data=self.data)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "Verify your account")
        self.assertEqual(client.content, b"OK")
        url = re.search("(?P<url>https?://[^\s]+)", mail.outbox[0].body).group("url")
        client = self.client.get(url)
        user = User.objects.get(username=self.data["username"])
        self.assertEqual(client.status_code, 301)
        self.assertTrue(user.is_verified, True)

    def user_created_but_not_verify_email(self):
        client = self.client.post(reverse("users:registration"), data=self.data)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].subject, "Verify your account")
        self.assertEqual(client.content, b"OK")
        user = User.objects.get(username=self.data["username"])
        self.assertTrue(user.is_verified, False)

    def test_user_create_fail(self):
        url = reverse("users:registration")
        self.data["password2"] = ""
        client = self.client.post(url, data=self.data)
        self.assertIn(b"This field is required.", client.content)
        self.data["password2"] = "12345"
        client = self.client.post(url, data=self.data)
        self.assertIn(b"Passwords not match", client.content)
        self.data["username"] = "H"
        client = self.client.post(url, data=self.data)
        self.assertIn(b"Login must contains more than 3 symbols", client.content)


class UserAuthorizationAndLogOutTestCase(TestCase):
    fixtures = ["users.json"]

    def test_user_authorization(self):
        client = self.client.post(reverse('users:sign_in'), data={'username': 'alex', 'password': '123'})
        self.assertEqual(client.content, b"OK")

    def test_user_authorization_fail(self):
        client = self.client.post(reverse('users:sign_in'), {'username': 'oleg', 'password': '123'})
        self.assertNotEqual(client.content, b"OK")
