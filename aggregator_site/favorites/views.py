from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin


class FavoriteList(ListView, LoginRequiredMixin):
    template_name = "favorites/favorites.html"

    def get_queryset(self):
        return self.request.user.favorites.all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["favorites"] = context["object_list"]
        return context

