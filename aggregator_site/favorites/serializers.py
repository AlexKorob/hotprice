import logging
from .models import Favorite
from django.core.exceptions import ObjectDoesNotExist

from products.serializers import ProductSmallSerializer
from rest_framework import serializers


log = logging.getLogger("file")


class FavoriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Favorite
        fields = ("id", "product", "user")
        read_only_fields = ("user", )

    def validate_product(self, data):
        user = self.context["request"].user
        try:
            Favorite.objects.get(user=user, product=data)
            log.info(f"User - {user.username} tried add product - {data} to favorites, but this favorite was added")
            raise serializers.ValidationError("This product was added to favorites")
        except ObjectDoesNotExist:
            log.info(f"User - {user.username} add product - {data} to favorites")
            return data


class FavoriteReadSerializer(serializers.ModelSerializer):
    product = ProductSmallSerializer()

    class Meta:
        model = Favorite
        fields = ("id", "product", "user")
        read_only_fields = ("user", "product")