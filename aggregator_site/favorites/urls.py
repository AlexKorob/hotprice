from django.urls import path, include
from . import api_views, views
from rest_framework.routers import DefaultRouter

app_name = "favorites"

router = DefaultRouter()
router.register(r"", api_views.FavoriteViewSet, basename="favorite")

urlpatterns = [
    path("", views.FavoriteList.as_view(), name="favorites_list"),
]
