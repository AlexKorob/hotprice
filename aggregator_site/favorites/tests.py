from django.test import TestCase
from products.models import Product
from django.urls import reverse
from django.contrib.auth import get_user_model
from .models import Favorite
from aggregator_site.base_tests import BaseTest, BaseTestAPI

from rest_framework.test import APITestCase


PRODUCT_ID = 1


class UserActionWithFavoritesTestCase(TestCase, BaseTest):
    fixtures = ["products.json", "start_category.json"]

    def setUp(self):
        self.auth()

    def create_two_favorites(self):
        fav_1 = Favorite.objects.create(user=self.user, product_id=1)
        fav_2 = Favorite.objects.create(user=self.user, product_id=2)
        return fav_1, fav_2

    def test_add_two_favorites(self):
        products = Product.objects.filter(id__in=(1, 2))
        product_1, product_2 = products[0], products[1]
        response = self.client.post(reverse("favorite-list"), data={"product": product_1.id})
        self.assertEqual(response.status_code, 201)
        response = self.client.post(reverse("favorite-list"), data={"product": product_2.id})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Favorite.objects.filter(user=self.user).count(), 2)

    def test_read_favorites(self):
        favorite_1, favorite_2 = self.create_two_favorites()
        response = self.client.get(reverse("favorites:favorites_list"))
        self.assertEqual(response.status_code, 200)
        self.assertIn(favorite_1.product.name, response.content.decode("utf-8"))
        self.assertIn(favorite_2.product.name, response.content.decode("utf-8"))

    def test_delete_one_favorite(self):
        favorite_1, favorite_2 = self.create_two_favorites()
        response = self.client.delete(reverse("favorite-detail", kwargs={"pk": favorite_1.id}))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Favorite.objects.filter(user=self.user).count(), 1)

    def test_add_not_exist_product(self):
        response = self.client.post(reverse("favorite-list"), data={"product": 100})
        self.assertEqual(response.status_code, 400)

    def test_add_duplicate_product(self):
        self.client.post(reverse("favorite-list"), data={"product": 1})
        response = self.client.post(reverse("favorite-list"), data={"product": 1})
        self.assertEqual(response.status_code, 400)

    def test_delete_not_exist_favorite(self):
        response = self.client.delete(reverse("favorite-detail", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 404)


class FavoritesCreateAPITestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    def setUp(self):
        self.auth("user")

    def test_user_add_favorite(self):
        response = self.client.post(reverse("favorite-list"), data={"product": PRODUCT_ID})
        self.assertEqual(response.status_code, 201)
        self.assertTrue(Favorite.objects.filter(product_id=PRODUCT_ID).exists())

    def test_user_add_duplicate_favorite(self):
        for i in range(2):
            response = self.client.post(reverse("favorite-list"), data={"product": PRODUCT_ID})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(Favorite.objects.filter(product_id=PRODUCT_ID).count(), 1)

    def test_user_add_not_exist_product_to_favorite(self):
        response = self.client.post(reverse("favorite-list"), data={"product": 10000})
        self.assertEqual(response.status_code, 400)
        self.assertFalse(Favorite.objects.filter(product_id=10000).exists())


class FavoritesReadAPITestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    def setUp(self):
        self.auth()
        self.create_favorites()

    def test_get_all_own_favorites(self):
        response = self.client.get(reverse("favorite-list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], len(self.products_id))

    def test_get_one_own_favorite(self):
        favorite_id = Favorite.objects.first().id
        response = self.client.get(reverse("favorite-detail", kwargs={"pk": favorite_id}))
        self.assertEqual(response.status_code, 200)

    def test_get_not_exist_favorite(self):
        not_exist_favorite_id = 9999
        response = self.client.get(reverse("favorite-detail", kwargs={"pk": not_exist_favorite_id}))
        self.assertEqual(response.status_code, 404)


class FavoriteUpdateAPITestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    def setUp(self):
        self.auth("user")
        self.create_favorites()

    def test_user_update_favorite(self):
        favorite_id = Favorite.objects.first().id
        new_product_id = 5
        response = self.client.patch(reverse("favorite-detail", kwargs={"pk": favorite_id}),
                                     data={"product": new_product_id})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Favorite.objects.get(pk=favorite_id).product_id, new_product_id)

        response = self.client.put(reverse("favorite-detail", kwargs={"pk": favorite_id}),
                                   data={"product": 6})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Favorite.objects.get(pk=favorite_id).product_id, 6)

    def test_user_update_not_exist_favorite(self):
        favorite_not_exist_id = Favorite.objects.last().id + 99999
        new_product_id = 5
        response = self.client.patch(reverse("favorite-detail", kwargs={"pk": favorite_not_exist_id}),
                                     data={"product": new_product_id})
        self.assertEqual(response.status_code, 404)
        response = self.client.put(reverse("favorite-detail", kwargs={"pk": favorite_not_exist_id}),
                                   data={"product": new_product_id})
        self.assertEqual(response.status_code, 404)


class FavoriteDeleteAPITestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    def setUp(self):
        self.auth("user")
        self.create_favorites()

    def test_user_delete_favorite(self):
        count_favorites = Favorite.objects.count()
        favorite_id = Favorite.objects.last().id
        response = self.client.delete(reverse("favorite-detail", kwargs={"pk": favorite_id}))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(count_favorites - 1, Favorite.objects.count())

    def test_user_delete_not_exist_favorite(self):
        count_favorites = Favorite.objects.count()
        not_exist_favorite_id = Favorite.objects.last().id + 1
        response = self.client.delete(reverse("favorite-detail", kwargs={"pk": not_exist_favorite_id}))
        self.assertEqual(response.status_code, 404)
        self.assertEqual(count_favorites, Favorite.objects.count())
