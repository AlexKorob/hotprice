from rest_framework.response import Response
from rest_framework import viewsets, permissions
from .serializers import FavoriteSerializer, FavoriteReadSerializer
from .models import Favorite


class FavoriteViewSet(viewsets.ModelViewSet):
    """
        list: << show you all own favorites >>
        create: parameters: input product id, which your are want add to favorite
        update: { input your own favorite id } parameters: id product which you are want update;
        delete: { input your own favorite id }
    """
    permission_classes = (permissions.IsAuthenticated, )

    def get_serializer_class(self):
        if self.request.method in permissions.SAFE_METHODS:
            return FavoriteReadSerializer
        return FavoriteSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return Favorite.objects.filter(user=self.request.user)