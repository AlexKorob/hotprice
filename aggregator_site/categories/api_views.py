from rest_framework.response import Response
from rest_framework import viewsets, permissions
from .permissions import IsAdminOrReadOnly
from .serializers import CategorySerializer, CategoryCreateUpdateDeleteSerializer
from .models import Category


class CategoryViewSet(viewsets.ModelViewSet):
    # serializer_class = CategorySerializer
    queryset = Category.objects.all()
    permission_classes = (IsAdminOrReadOnly, permissions.IsAuthenticatedOrReadOnly)

    def get_serializer_class(self):
        if self.request.method not in permissions.SAFE_METHODS:
            return CategoryCreateUpdateDeleteSerializer
        return CategorySerializer
