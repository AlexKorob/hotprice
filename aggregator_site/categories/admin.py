from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin, TreeRelatedFieldListFilter
from .models import Category


class CategoryAdmin(DraggableMPTTAdmin):
    list_filter = (("parent", TreeRelatedFieldListFilter), )


admin.site.register(Category, CategoryAdmin)
