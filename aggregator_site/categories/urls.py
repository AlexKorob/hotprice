from django.urls import path
from . import views, api_views
from rest_framework.routers import DefaultRouter

app_name = "categories"

router = DefaultRouter()
router.register(r"", api_views.CategoryViewSet)

urlpatterns = [

]
