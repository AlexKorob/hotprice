from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Category(MPTTModel):
    name = models.CharField(max_length=70, db_index=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name_plural = "categories"
        unique_together = (('name', 'parent', ), )

    def __str__(self):
        return self.name

    def get_full_path(self):
        return ' - '.join(self.get_ancestors(include_self=True).values_list('name', flat=True))

    @property
    def all_ancestors(self):
        return "\n".join(self.get_ancestors().values_list('name', flat=True))
