from rest_framework import serializers
from .models import Category
from aggregator_site.serializer_fields import RecursiveField


class CategorySerializer(serializers.ModelSerializer):
    children = RecursiveField(field="parent", many=True)

    class Meta:
        model = Category
        fields = ("id", "name", "parent_id", "children")


class CategoryCreateUpdateDeleteSerializer(serializers.ModelSerializer):
    parent_id = serializers.IntegerField(required=True)

    class Meta:
        model = Category
        fields = ("name", "parent_id")
