from django.urls import reverse
from aggregator_site.base_tests import BaseTestAPI

from rest_framework.test import APITestCase

from categories.models import Category


category_id = 253


class CategoryCreateTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", )

    def test_admin_create_category(self):
        self.auth("admin")
        num_categories = Category.objects.count()
        name = "SOME_CATEGORY"
        response = self.client.post(reverse("category-list"), data={"name": name,
                                                                    "parent_id": category_id})

        self.assertEqual(response.status_code, 201)
        self.assertIn(name, list(response.data.values()))
        self.assertEqual(Category.objects.count(), num_categories + 1)

    def test_simple_user_create_category(self):
        self.auth("user")
        num_categories = Category.objects.count()
        response = self.client.post(reverse("category-list"), data={"name": "SOME_CATEGORY",
                                                                    "parent_id": category_id})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Category.objects.count(), num_categories)


class CategoryReadTestCase(APITestCase):
    fixtures = ("start_category.json", )

    def test_get_all_categories(self):
        num_categories = Category.objects.count()
        response = self.client.get(reverse("category-list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], num_categories)

    def test_get_one_category(self):
        response = self.client.get(reverse("category-detail", args=[category_id]))
        category = Category.objects.get(id=category_id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["name"], category.name)


class CategoryUpdateTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", )
    new_category_name = "SOME_CATEGORY_NAME"
    new_category_parent_id = 250

    def test_admin_partial_update_category(self):
        self.auth("admin")
        new_name = "CATEGORY"
        parent_id = 250
        response = self.client.patch(reverse("category-detail", args=[category_id, ]),
                                     data={"name": new_name})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Category.objects.get(id=category_id).name, new_name)

        response = self.client.patch(reverse("category-detail", args=[category_id, ]),
                                     data={"parent_id": parent_id})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["parent_id"], Category.objects.get(id=category_id).parent_id)

    def test_admin_update_category(self):
        self.auth("admin")
        response = self.client.put(reverse("category-detail", args=[category_id, ]),
                                   data={"name": self.new_category_name,
                                         "parent_id": self.new_category_parent_id})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["name"], self.new_category_name)
        self.assertEqual(response.data["parent_id"], self.new_category_parent_id)

    def test_simple_user_partial_update_category(self):
        self.auth("user")
        response = self.client.patch(reverse("category-detail", args=[category_id, ]),
                                     data={"parent_id": 250})
        self.assertEqual(response.status_code, 403)

    def test_simple_user_update_category(self):
        self.auth("user")
        num_categories = Category.objects.count()
        response = self.client.put(reverse("category-detail", args=[category_id, ]),
                                   data={"name": self.new_category_name,
                                         "parent_id": self.new_category_parent_id})
        self.assertEqual(response.status_code, 403)


class CategoryDeleteTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", )

    def test_admin_delete_category(self):
        self.auth("admin")
        num_categories = Category.objects.count()
        response = self.client.delete(reverse("category-detail", args=[category_id, ]))
        self.assertTrue(response.status_code == 204)
        self.assertTrue((num_categories - 1) == Category.objects.count())

    def test_simple_user_delete_category(self):
        self.auth("user")
        num_categories = Category.objects.count()
        response = self.client.delete(reverse("category-detail", args=[category_id, ]))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Category.objects.count(), num_categories)
