from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from mptt.models import TreeForeignKey
from django.contrib.auth import get_user_model
from django.utils.text import slugify
from django.db.models import Avg


class Store(models.Model):
    name = models.CharField(max_length=40)


class SourceProduct(models.Model):
    name = models.CharField(max_length=255, db_index=True)
    price = models.DecimalField(decimal_places=2, max_digits=16)
    rating = models.DecimalField(decimal_places=1, max_digits=3)
    store = models.ForeignKey(Store, on_delete=models.SET_NULL, null=True)
    url_in_store = models.SlugField(max_length=255)
    description = models.TextField(max_length=2000)
    product = models.ForeignKey("Product", on_delete=models.CASCADE, related_name="source_products")

    def __str__(self):
        return f"{self.name} | price: {self.price} | {self.store.name}"


class ReviewSourceProduct(models.Model):
    source_product = models.ForeignKey(SourceProduct, on_delete=models.CASCADE, related_name="reviews_source")
    username = models.CharField(max_length=60)
    text = models.TextField(max_length=1000)
    advantages = models.TextField(max_length=255, blank=True)
    disadvantages = models.TextField(max_length=255, blank=True)
    date = models.CharField(max_length=30, blank=True)


class Product(models.Model):
    name = models.CharField(max_length=255, db_index=True)
    category = TreeForeignKey("categories.Category", on_delete=models.CASCADE, related_name="products")
    features = models.ManyToManyField("Feature", related_name="products")
    average_price = models.ManyToManyField("AveragePrice", related_name="products")
    description = models.TextField(max_length=2000)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateField(auto_now=True)
    # slug = models.SlugField(max_length=150, unique=True, blank=True, verbose_name="url")
    #
    # def save(self):
    #     self.slug = self.gen_slug(self.name)
    #     super().save()

    class Meta:
        ordering = ("-name", )

    def __str__(self):
        return f"Product {self.name} | in category {self.category.get_full_path()}"

    @property
    def rating(self):
        rating = self.rating_products.aggregate(Avg("rating"))["rating__avg"]
        if rating is None:
            return 0
        return rating


class RatingProduct(models.Model):
    rating = models.IntegerField(validators=(MinValueValidator(1), MaxValueValidator(5)))
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="rating_products")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="rating_products")


class ReviewProduct(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name="reviews")
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="reviews")
    text = models.TextField(max_length=1000)
    advantages = models.TextField(max_length=255, blank=True)
    disadvantages = models.TextField(max_length=255, blank=True)
    date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return f"Username - {self.user.username} | review {self.text[:15]}..."


class AveragePrice(models.Model):
    price = models.DecimalField(decimal_places=2, max_digits=18)
    date = models.DateTimeField(auto_now_add=True)


class Feature(models.Model):
    feature = models.CharField(max_length=60, db_index=True, blank=True)
    value = models.CharField(max_length=500)


class Image(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name="images")
    name = models.CharField(max_length=255)
