from rest_framework.validators import UniqueValidator

from .models import (ReviewProduct, RatingProduct, Product, SourceProduct, Store,
                     AveragePrice, Feature, Image)
from categories.serializers import CategorySerializer
from categories.models import Category
from django.core.exceptions import ObjectDoesNotExist
from .utils import get_min_max_price as get_prices

from rest_framework import serializers


class ReviewSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()

    class Meta:
        model = ReviewProduct
        fields = ("user", "product", "text", "advantages", "disadvantages", "date")
        read_only_fields = ("user", )


class ReviewCreateSerializer(serializers.ModelSerializer):
    date = serializers.ReadOnlyField()

    class Meta:
        model = ReviewProduct
        fields = ("user", "product", "text", "advantages", "disadvantages", "date")


class RatingSerializer(serializers.ModelSerializer):

    class Meta:
        model = RatingProduct
        fields = ("rating", "product", "user")
        read_only_fields = ("user", )

    def validate_product(self, product):
        user = self.context["request"].user
        try:
            RatingProduct.objects.get(user=user, product=product)
            raise serializers.ValidationError("You rated this product")
        except ObjectDoesNotExist:
            return product


class SourceProductCreateUpdateDeleteSerializer(serializers.ModelSerializer):
    store = serializers.SlugRelatedField(slug_field="name", queryset=Store.objects.all())
    url_in_store = serializers.URLField(max_length=700)

    class Meta:
        model = SourceProduct
        fields = ("id", "price", "rating", "store", "url_in_store", "description", "product")

    def validate_store(self, store):
        product_id = self.initial_data["product"]
        try:
            SourceProduct.objects.get(store=store, product_id=product_id)
            raise serializers.ValidationError("This store was added to product")
        except ObjectDoesNotExist:
            return store


class SourceProductReadSerializer(serializers.ModelSerializer):
    store = serializers.SlugRelatedField(slug_field='name', queryset=Store.objects.all())

    class Meta:
        model = SourceProduct
        fields = ("name", "price", "rating", "store", "url_in_store", "description")


class AveragePriceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AveragePrice
        fields = ("price", "date")


class FeatureSerializer(serializers.ModelSerializer):
    class Meta:
        model = Feature
        fields = ("feature", "value")


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ("name", )


class ProductCreateUpdateDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ("name", "average_price", "category", "features",
                  "description")


class ProductSerializer(serializers.ModelSerializer):
    source_products = SourceProductReadSerializer(many=True, read_only=True)
    breadcrumbs = serializers.CharField(source="category.get_full_path",
                                        read_only=True)
    reviews = ReviewSerializer(many=True, read_only=True)
    category = CategorySerializer()
    average_price = AveragePriceSerializer(many=True)
    features = FeatureSerializer(many=True)
    images = ImageSerializer(many=True)

    class Meta:
        model = Product
        fields = ("id", "name", "category", "breadcrumbs", "source_products",
                  "average_price", "images", "features", "description",
                  "created_on", "updated_on", "rating", "reviews")

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.description = validated_data.get("name", instance.description)
        category = validated_data.get("category", instance.category)
        instance.category = Category.objects.get(name=category.get("name"),
                                                 parent=category.get("parent").get("id"))
        instance.save()
        features = validated_data.get("features")
        if features:
            instance.features.clear()
            for feature in features:
                feature_obj, _ = Feature.objects.get_or_create(feature=feature.get("feature"),
                                                               value=feature.get("value"))
                instance.features.add(feature_obj)

        return instance


class ProductSmallSerializer(serializers.ModelSerializer):
    images = ImageSerializer()
    min_max_price = serializers.SerializerMethodField(read_only=True)
    category = CategorySerializer()

    class Meta:
        model = Product
        fields = ("id", "name", "images", "min_max_price", "category")

    def get_min_max_price(self, product):
        return get_prices(product)
