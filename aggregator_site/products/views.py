import json
import logging

from django.http import HttpResponse
from django.shortcuts import render, reverse
from django.views.generic import View, TemplateView, DetailView, CreateView, ListView
from django.shortcuts import render
from categories.models import Category
from haystack.backends import SQ
from haystack.forms import ModelSearchForm
from haystack.inputs import AutoQuery
from haystack.query import SearchQuerySet

from .models import Product, SourceProduct, RatingProduct
from .forms import ReviewForm, AddRatingForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from .utils import get_min_max_price, get_images, paginator, pretty_price, SearchAndProductsFromCategoryMixin
from haystack.generic_views import SearchView
from django.views import View


log = logging.getLogger("file")


class CustomModelSearchFrom(ModelSearchForm):
    def search(self):
        if not self.is_valid():
            return self.no_query_found()

        if not self.cleaned_data.get('q'):
            return self.no_query_found()

        q = self.cleaned_data['q']
        sqs = self.searchqueryset.filter(SQ(name__contains=AutoQuery(q)) |
                                         SQ(category=AutoQuery(q)) |
                                         SQ(description=AutoQuery(q)) |
                                         SQ(categories_ancestors=AutoQuery(q)))

        if self.load_all:
            sqs = sqs.load_all()
        return [result for result in sqs.highlight() if result.score > 0.03]


def autocomplete(request):
    q = request.GET.get('q', '')
    if q:
        sqs = SearchQuerySet().autocomplete(content_auto=q)[:5]

        suggestions = [{"name": result.name, "id": result.pk, "score": result.score} for result in sqs]
        for i in range(len(suggestions) - 1):
            for j in range(len(suggestions) - 1):
                if suggestions[j]["name"] > suggestions[j + 1]["name"]:
                    tmp = suggestions[j]
                    suggestions[j] = suggestions[j + 1]
                    suggestions[j + 1] = tmp
    else:
        suggestions = ""
    the_data = json.dumps({
        'results': suggestions
    })

    return HttpResponse(the_data, content_type='application/json')


class Search(SearchView):
    template_name = "products/index.html"
    paginate_by = 25
    form_class = CustomModelSearchFrom

    def get_context_data(self, *args, **kwargs):

        context = super(Search, self).get_context_data(*args, **kwargs)
        q = self.request.GET["q"]
        x_forwarded_for = self.request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip = x_forwarded_for.split(',')[0]
        else:
            ip = self.request.META.get('REMOTE_ADDR')
        user = self.request.user
        if user:
            username = user.username
        else:
            username = "User"

        log.info(f"{username} with ip - {ip}, search - '{q}'")

        search_query_set = context["object_list"]
        products = [Product.objects.get(id=prod.pk) for prod in search_query_set]

        page = context["page_obj"]
        context["is_paginator"] = context["is_paginated"]
        context["categories"] = Category.objects.filter(parent=None)
        paginator = context["paginator"]

        context["next_url"] = f"?page={page.number+1}&q={self.request.GET['q']}" \
                              if page.number + 1 <= paginator.num_pages else ""
        context["prev_url"] = f"?page={page.number-1}&q={self.request.GET['q']}" if page.number - 1 > 0 else ""

        images = get_images(products)
        prices = get_min_max_price(products)
        pretty_prices = []

        for low_price, hight_price in prices:
            pretty_prices.append((pretty_price(low_price), pretty_price(hight_price)))
        if products:
            products = zip(products, images, pretty_prices)
        else:
            products = False
            context["not_found"] = True
        context["page"] = page
        context["products"] = products

        return context


class Index(ListView):
    queryset = Category.objects.filter(parent=None)
    template_name = "products/index.html"
    context_object_name = "categories"

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        return context


class ProductsFromCategory(SearchAndProductsFromCategoryMixin, ListView):
    template_name = "products/index.html"
    view_name = "ProductsFromCategory"


class ProductDetail(DetailView):
    template_name = "products/product_detail.html"
    model = Product

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product = context["product"]

        breadcrumb = Category.objects.get(name=product.category.name, parent=product.category.parent).get_ancestors(include_self=True)

        source_products = SourceProduct.objects.filter(product=product)
        context["images"] = get_images(product)
        context["price"] = get_min_max_price(product)

        pretty_price_products = []
        for source_product in source_products:
            pretty_price_products.append(pretty_price(source_product.price))
        source_products_n_pretty_price = zip(source_products, pretty_price_products)
        if self.request.user.is_authenticated:
            if not RatingProduct.objects.filter(product=product, user=self.request.user).first():
                context["rating_input"] = AddRatingForm()

        context["product"] = product
        context["breadcrumb"] = breadcrumb
        context["source_products_n_pretty_price"] = source_products_n_pretty_price
        return context
