from django.template import Library

register = Library()

@register.filter
def key_value(dict, key):
    try:
        value = dict[key]
    except KeyError:
        value = ""
    return value
