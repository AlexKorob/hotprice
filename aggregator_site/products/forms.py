from django import forms
from .models import ReviewProduct
from django.forms import ModelForm
from django_starfield import Stars


class AddRatingForm(forms.Form):
    rating = forms.IntegerField(widget=Stars, label="")


class ReviewForm(ModelForm):
    class Meta:
        model = ReviewProduct
        fields = ("user", "product", "text", "advantages", "disadvantages")
