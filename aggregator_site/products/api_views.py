from django.contrib.auth.models import Group
from rest_framework import generics, status, permissions, viewsets
from rest_framework.response import Response
from .serializers import (ReviewSerializer, RatingSerializer, ProductSerializer,
                          ProductCreateUpdateDeleteSerializer,
                          SourceProductReadSerializer,
                          SourceProductCreateUpdateDeleteSerializer,
                          ReviewCreateSerializer)
from .models import RatingProduct, ReviewProduct, Product, SourceProduct
from .forms import ReviewForm
from .permissions import (IsModeratorORSuperuser, IsAdminDelete, IsModeratorUpdate,
                          IsUserCreate, IsModeratorUpdateIsAdminDeleteIsUserCreate)


class RatingListCreate(generics.ListCreateAPIView):
    serializer_class = RatingSerializer
    permission_classes = (permissions.IsAuthenticated, )

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        return RatingProduct.objects.filter(user=self.request.user)


class ReviewViewSet(viewsets.ModelViewSet):
    serializer_class = ReviewSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsModeratorUpdateIsAdminDeleteIsUserCreate)

    def get_queryset(self):
        if self.request.user.groups.filter(name="Moderators").exists() or \
                self.request.user.is_superuser:
            return ReviewProduct.objects.all()
        return ReviewProduct.objects.filter(user=self.request.user)

    def get_serializer_class(self):
        # if self.request.method not in permissions.SAFE_METHODS:
        #     return ReviewCreateSerializer
        return ReviewSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


# class ReviewListCreate(generics.ListCreateAPIView):
#     serializer_class = ReviewSerializer
#     permission_classes = (permissions.IsAuthenticated, )
#
#     def create(self, request, *args, **kwargs):
#         data = dict(request.data)
#         normalizated_data = {}
#
#         for key, val in data.items():
#             normalizated_data[key] = val[0]
#         normalizated_data["user"] = request.user.id
#         bound_form = ReviewForm(normalizated_data)
#
#         if bound_form.is_valid():
#             serializer = self.get_serializer(data=normalizated_data)
#             serializer.is_valid(raise_exception=True)
#             self.perform_create(serializer)
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#
#         return Response({"errors": bound_form.errors.as_ul()}, status=status.HTTP_200_OK)
#
#     def perform_create(self, serializer):
#         serializer.save(user=self.request.user)
#
#     def get_queryset(self):
#         return ReviewProduct.objects.filter(user=self.request.user)


class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsModeratorORSuperuser)

    def get_serializer_class(self):
        if self.request.method not in permissions.SAFE_METHODS:
            return ProductCreateUpdateDeleteSerializer
        return ProductSerializer


class SourceProductViewSet(viewsets.ModelViewSet):
    queryset = SourceProduct.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsModeratorORSuperuser)

    def get_serializer_class(self):
        if self.request.method not in permissions.SAFE_METHODS:
            return SourceProductCreateUpdateDeleteSerializer
        return SourceProductReadSerializer
