from django.urls import path, include
from . import views, api_views

app_name = "products"

urlpatterns = [
    path("", views.Index.as_view(), name="index"),
    path("category/<int:category_id>", views.ProductsFromCategory.as_view(), name="products_from_category"),
    path("detail/<int:pk>/", views.ProductDetail.as_view(), name="product_detail"),
    path("search/", views.Search.as_view(), name="search"),
    path("search/autocomplete/", views.autocomplete),

    # path("api/reviews/", api_views.ReviewListCreate.as_view(), name="review"),
    path("api/ratings/", api_views.RatingListCreate.as_view(), name="rating"),
]
