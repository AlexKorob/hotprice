import logging
from .models import Image, SourceProduct, Product
from categories.models import Category
from django.core.files.storage import default_storage
from django.core.paginator import Paginator
from django.core.exceptions import ObjectDoesNotExist


log = logging.getLogger("file")


def get_images(products):
    one_item = False
    if isinstance(products, Product):
        products = [products, ]
        one_item = True
    images = []
    for product in products:
        category_name = product.category.name
        category_name = category_name.replace(" ", "_")
        product_images_url = Image.objects.filter(product=product)
        product_images = []
        if not product_images_url:
            product_images.append(default_storage.url("noimage.jpg"))
        for img in product_images_url:
            product_images.append(default_storage.url(category_name + "/" + img.name))
        images.append(product_images)

    if one_item:
        return images[0]
    return images


def pretty_price(price):
    format_price = ""
    count = 0
    price = str(price)
    if "." in price:
        end = price[price.find("."):]
        if end[1] == "0":
            end = ""
        start = price[:price.find(".")]
    else: start = price; end = ""
    for i in reversed(start):
        count += 1
        if count == 3:
            format_price = " " + i + format_price
            count = 0
            continue
        format_price = i + format_price
    return format_price.strip() + end


def get_min_max_price(products):
    one_item = False
    if isinstance(products, Product):
        products = [products, ]
        one_item = True
    prices = []
    for product in products:
        price = [float(source_product.price) if str(source_product.price)[-2] == "0" else
                 float(source_product.price) for source_product in product.source_products.all()]
        if not price:
            price = [0]
        prices.append((min(price), max(price)))

    if one_item:
        return prices[0]
    return prices


def paginator(request, products):
    paginator = Paginator(products, 25)
    number_page = request.GET.get('page', 1)
    page = paginator.get_page(number_page)
    is_paginator = page.has_other_pages()
    search = request.GET.get("q", "")

    if search:
        search = "&q=" + search
    else:
        search = ""

    if page.has_previous():
        prev_url = '?page=' + str(page.number-1) + search
    else:
        prev_url = ''

    if page.has_next():
        next_url = '?page=' + str(page.number+1) + search
    else:
        next_url = ''
    return page, next_url, prev_url, is_paginator


def filter_products(filters, products):
    features_filter = set((key, val) for key, values in filters.items() for val in values
                          if key != "prices" and key != "filters" and key != "filter-submit-button")

    lower_price, higher_price = filters.get("prices")
    total_products = []
    for product in products:
        product_features = set(product.features.values_list("feature", "value"))
        min_price, max_price = get_min_max_price(product)
        if (features_filter.intersection(product_features) or bool(features_filter) is False)\
                and min_price >= lower_price and max_price <= higher_price:
            total_products.append(product)
    return total_products


def build_filters(products):
    def can_use_feature(feature, value):
        if feature.startswith("Габарит") or feature.startswith("габарит") or len(value) > 30 or \
           feature.startswith("Размер") or feature.startswith("размер"):
            return False
        return True

    features = {}
    prices = get_min_max_price(products)
    filters = {"count_filter_products": products.count(), "total_count_product": products.count(),
                "max_price": max(prices), "min_price": min(prices)}
    for product in products:
        prod_features = product.features.all()
        for prod_feature in prod_features:
            feature = prod_feature.feature
            value = prod_feature.value
            if value not in features.get(feature, ""):
                if can_use_feature(feature, value):
                    features.setdefault(feature, []).append(value)

    temp_features = {}
    for key, value in features.items():
        if len(value) > 1:
            temp_features[key] = value
    filters["features"] = temp_features
    return filters


class SearchAndProductsFromCategoryMixin(object):
    views_names = ("Search", "ProductsFromCategory")
    view_name = None

    def get_queryset(self):
        if self.view_name == self.views_names[1]:
            category = Category.objects.get(id=self.kwargs["category_id"])
            return Product.objects.filter(category__in=category.get_descendants(include_self=True))
        query = self.request.GET.get("q")
        products = Product.objects.filter(name__icontains=query)

        if not products:
            try:
                products = Product.objects.filter(category__in=Category.objects.get(name__iexact=query).\
                                                  get_descendants(include_self=True))
            except ObjectDoesNotExist as e:
                log.error(e)
        return products

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        products = context["object_list"]
        category = None

        if self.view_name == self.views_names[1]:
            category = Category.objects.get(id=self.kwargs["category_id"])
            context["breadcrumb"] = category.get_ancestors(include_self=True)
            categories = Category.objects.filter(parent=category, level=category.level + 1)
            if not categories:
                categories = Category.objects.filter(parent=category.parent)
            context["categories"] = categories
            context["filters_values"] = self.kwargs.get("filters_values")
        else:
            context["categories"] = Category.objects.filter(parent=None)

        if category and not category.parent is None and not category.get_ancestors().count() == 0:
            context["filter"] = build_filters(products)
            self.template_name = "products/filters.html"

        if self.request.GET.get("filters"):
            filters = dict(self.request.GET)
            filters["prices"] = [float(price) for price in filters.get("prices", "0-0")[0].split("-")]
            context["filters_bound"] = filters
            products = filter_products(filters, products)
            context["filter"]["count_filter_products"] = len(products)

        page, context["next_url"], context["prev_url"], context["is_paginator"] = paginator(self.request, products)
        images = get_images(page)
        prices = get_min_max_price(page)
        pretty_prices = []

        for low_price, hight_price in prices:
            pretty_prices.append((pretty_price(low_price), pretty_price(hight_price)))
        if products:
            products = zip(page, images, pretty_prices)
        else:
            products = False
            context["not_found"] = True
        context["page"] = page
        context["products"] = products

        return context
