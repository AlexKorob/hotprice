from rest_framework import permissions


class IsModeratorORSuperuser(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.groups.filter(name='Moderators').exists() \
                or request.user.is_superuser \
                or request.method in permissions.SAFE_METHODS:
            return True
        return False


class IsModeratorUpdateIsAdminDeleteIsUserCreate(permissions.BasePermission):
    def has_permission(self, request, view):
        is_admin = request.user.is_superuser
        is_moderator = request.user.groups.filter(name='Moderators').exists()
        if request.method in permissions.SAFE_METHODS:
            return True
        # elif is_admin:
        #     return True
        elif is_admin and request.method == "DELETE":
            return True
        elif is_moderator and request.method in ("PUT", "PATCH"):
            return True
        elif (not is_moderator) and (not is_admin):
            return True
        return False


class IsAdminDelete(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser and request.method in ("DELETE", ):
            return True
        return False


class IsModeratorUpdate(permissions.BasePermission):
    def has_permission(self, request, view):
        is_moderator = request.user.groups.filter(name='Moderators').exists()
        if is_moderator and request.method in ("PUT", "PATCH"):
            return True
        return False


class IsUserCreate(permissions.BasePermission):
    def has_permission(self, request, view):
        is_moderator = request.user.groups.filter(name='Moderators').exists()
        is_superuser = request.user.is_superuser
        if (not is_moderator) and (not is_superuser):
            return True
        return False
