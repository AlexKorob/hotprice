from . import views, api_views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r"source-products", api_views.SourceProductViewSet, basename="source_product")
router.register(r"review", api_views.ReviewViewSet, basename="review")
router.register(r"", api_views.ProductViewSet, basename="product")

urlpatterns = router.urls
