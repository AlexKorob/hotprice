from unittest.mock import patch, Mock

from django.test import TestCase
from django.urls import reverse

from rest_framework.test import APITestCase

from categories.models import Category
from aggregator_site.base_tests import BaseTest, BaseTestAPI
from .utils import get_images, pretty_price, get_min_max_price, paginator
from .models import Product, RatingProduct, ReviewProduct, AveragePrice, Feature, Image,\
                    SourceProduct


class ProductTestCase(TestCase, BaseTest):
    fixtures = ["products.json", "start_category.json"]
    product_id = 5

    def test_show_all_products_with_choosed_category(self):
        self.auth()
        category = Category.objects.filter(parent=None)[1]
        products_from_category = Product.objects.filter(category__in=category.get_descendants(include_self=True))
        response = self.client.get(reverse("products:products_from_category", kwargs={"category_id": category.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTrue(products_from_category.count() >= 1)
        self.assertIn(products_from_category[0].name, response.content.decode("utf-8"))

    # def test_search_product(self):
    #     self.auth()
    #     for_search = "Посудомоечная машина Beko DTC36610W"
    #     response = self.client.get(reverse("products:search"), {"q": for_search})
    #     self.assertEqual(response.status_code, 200)
    #     self.assertIn(for_search, response.content.decode("utf-8"))

    def test_return_start_page(self):
        self.auth()
        response = self.client.get(reverse("products:index"))
        self.assertEqual(response.status_code, 200)

    def test_product_detail(self):
        self.auth()
        response = self.client.get(reverse("products:product_detail", kwargs={"pk": self.product_id}))
        self.assertEqual(response.status_code, 200)

    # def test_not_authorized_user_send_review(self):
    #     response = self.client.post(reverse("products:review"), data={"product": self.product_id,
    #                                                                        "advantages": "good",
    #                                                                        "disadvantages": "Nope",
    #                                                                        "text": "Good item"})
    #     self.assertEqual(response.status_code, 401)

    def test_not_authorized_user_send_rating(self):
        response = self.client.post(reverse("products:rating"), data={"rating": 5, "product": self.product_id})
        self.assertEqual(response.status_code, 401)

    def test_duplicate_send_rating(self):
        self.auth()
        response = self.client.post(reverse("products:rating"), data={"rating": 5, "product": self.product_id})
        self.assertEqual(response.status_code, 201)
        response = self.client.post(reverse("products:rating"), data={"rating": 2, "product": self.product_id})
        self.assertEqual(response.status_code, 400)

    # def test_send_review(self):
    #     self.auth()
    #     data = {"product": self.product_id, "advantages": "good", "disadvantages": "Nope", "text": "Good item"}
    #     response = self.client.post(reverse("products:review"), data=data)
    #     self.assertEqual(response.status_code, 201)
    #     review = ReviewProduct.objects.filter(user__username=self.user.username, product_id=self.product_id)
    #     self.assertEqual(review.count(), 1)
    #     self.assertEqual(review.first().user.username, self.user.username)

    def test_send_rating(self):
        self.auth()
        rating = 5
        data = {"rating": rating, "product": self.product_id}
        response = self.client.post(reverse("products:rating"), data=data)
        self.assertEqual(response.status_code, 201)
        product_rating = RatingProduct.objects.get(user__username=self.user.username, product_id=self.product_id)
        self.assertEqual(product_rating.rating, rating)


class TestProductsUtils(TestCase):

    @patch("products.models.Image.objects.filter")
    @patch("products.utils.default_storage.url", return_value="some_img")
    def test_get_images_for_products(self, mock_default_storage, mock_list_images_for_product_from_s3):
        mock_product_image = Mock()
        mock_product_image.name = "some_image"  # WARNING:  I can't pass "name" to kwargs, because "name" is used by Mock constructor
        mock_list_images_for_product_from_s3.return_value = [mock_product_image]
        products = []
        for i in range(3):
            product_mock_attr = {"category.name": str(i) + "_product"}
            products.append(Mock(**product_mock_attr))
        images = get_images(products)
        self.assertEqual(len(images), 3)

    def test_get_min_max_price_for_product(self):
        product = Mock()
        subproducts_prices = [200, 450.00, 653.9, 653.84, 12, 304.0]
        mock_sourceproducts = []
        for price in subproducts_prices:
            mock_sourceproducts.append(Mock(price=price))
        product.source_products.all.return_value = mock_sourceproducts
        prices = get_min_max_price([product])[0]
        min_price = prices[0]
        max_price = prices[1]
        self.assertEqual(min(subproducts_prices), min_price)
        self.assertEqual(max(subproducts_prices), max_price)

    def test_paginator(self):
        # paginator split products on 25 items on one page
        products = ["product_" + str(i) for i in range(50)]
        request = Mock(GET={"page": 1, "search": ""})
        page, next_url, prev_url, is_paginator = paginator(request, products)
        self.assertTrue(is_paginator)
        self.assertTrue(prev_url == "")
        self.assertEqual(next_url, "?page=2")
        self.assertEqual(len(list(page)), 25)
        request = Mock(GET={"page": 2, "search": ""})
        page, next_url, prev_url, is_paginator = paginator(request, products)
        self.assertTrue(prev_url == "?page=1")
        self.assertEqual(next_url, "")
        self.assertEqual(len(list(page)), 25)

    def test_pretty_price(self):
        self.assertEqual(pretty_price(2000.0), "2 000")
        self.assertEqual(pretty_price(20000.00), "20 000")
        self.assertEqual(pretty_price(200000), "200 000")
        self.assertEqual(pretty_price(2000000.00), "2 000 000")
        self.assertEqual(pretty_price(20000.34), "20 000.34")
        self.assertEqual(pretty_price(20.34), "20.34")


class ProductCreateAPITestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")
    category_id = Category.objects.first().id
    average_price_id = AveragePrice.objects.first().id
    feature_id = Feature.objects.first().id
    data = {
        "name": "item",
        "average_price": [
            average_price_id
        ],
        "category": category_id,

        "features": [
            feature_id
        ],
        "description": "description"
    }

    def test_moderator_create_product(self):
        self.auth("moderator")
        num_products = Product.objects.count()
        response = self.client.post(reverse("product-list"), data=self.data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Product.objects.count(), num_products + 1)
        self.assertTrue(Product.objects.get(name=self.data["name"]))

    def test_simple_user_create_product(self):
        self.auth("user")
        num_products = Product.objects.count()
        response = self.client.post(reverse("product-list"), data=self.data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Product.objects.count(), num_products)

    def test_superuser_create_product(self):
        self.auth("admin")
        num_products = Product.objects.count()
        response = self.client.post(reverse("product-list"), data=self.data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Product.objects.count(), num_products + 1)
        self.assertTrue(Product.objects.get(name=self.data["name"]))


class ProductReadAPITestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    def test_user_receive_products(self):
        count_products = Product.objects.count()
        response = self.client.get(reverse("product-list"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["count"], count_products)

    def test_user_receive_product(self):
        product_id = Product.objects.last().id
        response = self.client.get(reverse("product-detail", kwargs={"pk": product_id}))
        self.assertEqual(response.status_code, 200)

    def test_user_get_not_exist_product(self):
        not_exist_product_id = Product.objects.last().id + 99999
        response = self.client.get(reverse("product-detail", kwargs={"pk": not_exist_product_id}))
        self.assertEqual(response.status_code, 404)


class ProductUpdateAPITestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")
    category_id = Category.objects.first().id
    average_price_id = AveragePrice.objects.first().id
    feature_id = Feature.objects.first().id
    data = {
        "name": "item",
        "average_price": [
            average_price_id
        ],
        "category": category_id,

        "features": [
            feature_id
        ],
        "description": "description"
    }

    def test_simple_user_update_and_partial_update_product(self):
        self.auth("user")
        response = self.client.put(reverse("product-detail", kwargs={"pk": 1}), data=self.data)
        self.assertEqual(response.status_code, 403)
        response = self.client.patch(reverse("product-detail", kwargs={"pk": 1}), data=self.data)
        self.assertEqual(response.status_code, 403)

    def test_moderator_update_product(self):
        self.auth("moderator")
        response = self.client.put(reverse("product-detail", kwargs={"pk": 5}), data=self.data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(Product.objects.get(name=self.data["name"]))

    def test_moderator_partial_update_product(self):
        self.auth("moderator")
        new_name = "new_name"
        response = self.client.patch(reverse("product-detail", kwargs={"pk": 5}),
                                     data={"name": new_name, "category": self.category_id})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Product.objects.get(id=5).name, new_name)


class ProductDeleteAPITestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    def test_simple_user_delete_product(self):
        self.auth("user")
        num_products = Product.objects.count()
        response = self.client.delete(reverse("product-detail", kwargs={"pk": 5}))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Product.objects.count(), num_products)
        self.assertTrue(Product.objects.filter(id=5).exists())

    def test_moderator_delete_product(self):
        self.auth("moderator")
        num_products = Product.objects.count()
        response = self.client.delete(reverse("product-detail", kwargs={"pk": 5}))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(Product.objects.count(), num_products - 1)
        self.assertFalse(Product.objects.filter(id=5).exists())


class SourceProductCreateTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    data = {
        "price": 100,
        "rating": 4,
        "store": "neon",
        "url_in_store": "http://someurl.com/",
        "description": "DESCRIPTION",
        "product": 1
    }

    def test_moderator_create_source_product(self):
        self.auth("moderator")
        num_products = Product.objects.count()
        response = self.client.post(reverse("source_product-list"), data=self.data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(SourceProduct.objects.count(), num_products + 1)

    def test_simple_user_create_source_product(self):
        self.auth("user")
        num_products = SourceProduct.objects.count()
        response = self.client.post(reverse("source_product-list"), data=self.data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(SourceProduct.objects.count(), num_products)

    def test_moderator_create_source_product_with_duplicate_store_name(self):
        self.auth("moderator")
        response = self.client.post(reverse("source_product-list"), data=self.data)
        self.assertEqual(response.status_code, 201)
        response = self.client.post(reverse("source_product-list"), data=self.data)
        self.assertEqual(response.status_code, 400)


class SourceProductUpdateTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    data = {
        "price": 100,
        "rating": 4,
        "store": "neon",
        "url_in_store": "http://someurl.com/",
        "description": "DESCRIPTION",
        "product": 1
    }

    def test_moderator_update_source_product(self):
        self.auth("moderator")
        response = self.client.put(reverse("source_product-detail", kwargs={"pk": 1}),
                                   data=self.data
                                   )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(SourceProduct.objects.get(product_id=self.data["product"]).description,
                         self.data["description"]
                         )

        response = self.client.patch(reverse("source_product-detail", kwargs={"pk": 1}),
                                     data={"description": "some_description"}
                                     )
        self.assertEqual(response.status_code, 200)
        self.assertTrue(SourceProduct.objects.get(description="some_description"))

    def test_simple_user_update_source_product(self):
        self.auth("user")
        response = self.client.put(reverse("source_product-detail", kwargs={"pk": 1}),
                                   data=self.data
                                   )
        self.assertEqual(response.status_code, 403)

        response = self.client.patch(reverse("source_product-detail", kwargs={"pk": 1}),
                                     data={"name": "some_name"}
                                     )
        self.assertEqual(response.status_code, 403)


class SourceProductDeleteTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    def test_moderator_delete_source_product(self):
        self.auth("moderator")
        num_products = SourceProduct.objects.count()
        response = self.client.delete(reverse("source_product-detail", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(SourceProduct.objects.count(), num_products - 1)

    def test_simple_user_delete_source_product(self):
        self.auth("user")
        num_products = SourceProduct.objects.count()
        response = self.client.delete(reverse("source_product-detail", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(SourceProduct.objects.count(), num_products)


class ReviewCreateTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    data = {"product": 1,
            "user": 1,
            "text": "good item",
            "advantages": "good",
            "disadvantages": "none"
            }

    def test_simple_user_create_review(self):
        self.auth("user")
        response = self.client.post(reverse("review-list"), data=self.data)
        self.assertEqual(response.status_code, 201)
        self.assertTrue(ReviewProduct.objects.get(text=self.data["text"]))

    def test_not_auth_user_create_review(self):
        response = self.client.post(reverse("review-list"), data=self.data)
        self.assertEqual(response.status_code, 401)

    def test_moderator_create_review(self):
        self.auth("moderator")
        response = self.client.post(reverse("review-list"), data=self.data)
        self.assertEqual(response.status_code, 403)


class ReviewUpdateTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")
    data = {"product": 1,
            "text": "test",
            "user": 1,
            "advantages": "test",
            "disadvantages": "test"
            }

    def tearDown(self):
        self.logout()

    def test_simple_user_update_own_review(self):
        self.auth("user")
        self.create_review()
        response = self.client.put(reverse("review-detail", kwargs={"pk": 1}),
                                   data=self.data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(ReviewProduct.objects.get(text="test"))

        response = self.client.patch(reverse("review-detail", kwargs={"pk": 1}),
                                     data={"text": "test_test"})
        self.assertEqual(response.status_code, 200)
        self.assertTrue(ReviewProduct.objects.get(text="test_test"))

    def test_moderator_update_review(self):
        self.auth("moderator")
        self.create_review()
        response = self.client.put(reverse("review-detail", kwargs={"pk": 1}),
                                   data=self.data)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(ReviewProduct.objects.get(text="test"))

        response = self.client.patch(reverse("review-detail", kwargs={"pk": 1}),
                                     data={"text": "test test"})
        self.assertEqual(response.status_code, 200)
        self.assertTrue(ReviewProduct.objects.get(text="test test"))

    def test_admin_update_review(self):
        self.auth("admin")
        self.create_review()
        response = self.client.put(reverse("review-detail", kwargs={"pk": 1}),
                                   data=self.data)
        self.assertEqual(response.status_code, 403)
        response = self.client.patch(reverse("review-detail", kwargs={"pk": 1}),
                                     data={"text": "test test"})
        self.assertEqual(response.status_code, 403)


class ReviewDeleteTestCase(APITestCase, BaseTestAPI):
    fixtures = ("start_category.json", "products.json")

    def test_user_delete_own_review(self):
        self.auth("user")
        self.create_review()
        num_review = ReviewProduct.objects.count()
        response = self.client.delete(reverse("review-detail", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(ReviewProduct.objects.count(), num_review - 1)

    def test_admin_delete_review(self):
        self.auth("admin")
        self.create_review()
        num_review = ReviewProduct.objects.count()
        response = self.client.delete(reverse("review-detail", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(ReviewProduct.objects.count(), num_review - 1)

    def test_moderator_delete_review(self):
        self.auth("moderator")
        self.create_review()
        num_review = ReviewProduct.objects.count()
        response = self.client.delete(reverse("review-detail", kwargs={"pk": 1}))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(ReviewProduct.objects.count(), num_review)
