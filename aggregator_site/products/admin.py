from django.contrib import admin
from .models import Product
from mptt.admin import TreeRelatedFieldListFilter


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Product._meta.fields if field.name != "description"]
    list_filter = ("updated_on", ("category", TreeRelatedFieldListFilter))
    search_fields = ("name", "category__name", "features__feature", "description")
