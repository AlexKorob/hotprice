import datetime
from haystack import indexes
from .models import Product


class ProductIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    name = indexes.CharField(model_attr='name', boost=20)
    category = indexes.CharField(model_attr="category__name", boost=8)
    categories_ancestors = indexes.CharField(model_attr="category__all_ancestors")
    created_on = indexes.DateTimeField(model_attr='created_on')
    updated_on = indexes.DateField(model_attr="updated_on")
    description = indexes.CharField(model_attr="description", boost=5)

    # We add this for autocomplete.
    content_auto = indexes.EdgeNgramField(model_attr='name')

    def __str__(self):
        return self.__class__

    def get_model(self):
        return Product

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all()
