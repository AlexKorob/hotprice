$(function() {
  (function($) {

  $.fn.supposition = function() {
    var $w = $(window),
      /*do this once instead of every onBeforeShow call*/
      _offset = function(dir) {
        return window[dir == 'y' ? 'pageYOffset' : 'pageXOffset'] || document.documentElement && document.documentElement[dir == 'y' ? 'scrollTop' : 'scrollLeft'] || document.body[dir == 'y' ? 'scrollTop' : 'scrollLeft'];
      },
      onInit = function() {
        /* I haven't touched this bit - needs work as there are still z-index issues */
        $topNav = $('li', this);
        var cZ = parseInt($topNav.css('z-index')) + $topNav.length;
        $topNav.each(function() {
          $(this).css({
            zIndex: --cZ
          });
        });
      },
      onHide = function() {
        this.css({
          marginTop: '',
          marginLeft: ''
        });
      },
      onBeforeShow = function() {
        this.each(function() {
          var $u = $(this);
          $u.css('display', 'block');
          var menuWidth = $u.width(),
            parentWidth = $u.parents('ul').width(),
            totalRight = $w.width() + _offset('x'),
            menuRight = $u.offset().left + menuWidth;
          if (menuRight > totalRight) {
            $u.css('margin-left', ($u.parents('ul').length == 1 ? totalRight - menuRight : -(menuWidth + parentWidth)) + 'px');
          }

          var windowHeight = $w.height(),
            offsetTop = $u.offset().top,
            menuHeight = $u.height(),
            baseline = windowHeight + _offset('y');
          var expandUp = (offsetTop + menuHeight > baseline);
          if (expandUp) {
            $u.css('margin-top', baseline - (menuHeight + offsetTop));
          }
          $u.css('display', 'none');
        });
      };

    return this.each(function() {
      var $this = $(this),
        o = $this.data('sf-options'); /* get this menu's options */

      /* if callbacks already set, store them */
      var _onInit = o.onInit,
        _onBeforeShow = o.onBeforeShow,
        _onHide = o.onHide;

      $.extend($this.data('sf-options'), {
        onInit: function() {
          onInit.call(this); /* fire our Supposition callback */
          _onInit.call(this); /* fire stored callbacks */
        },
        onBeforeShow: function() {
          onBeforeShow.call(this); /* fire our Supposition callback */
          _onBeforeShow.call(this); /* fire stored callbacks */
        },
        onHide: function() {
          onHide.call(this); /* fire our Supposition callback */
          _onHide.call(this); /* fire stored callbacks */
        }
      });
    });
  };

  })(jQuery);
  $('.categories>ul.sf-menu').superfish({}).supposition();

  // slick
  $(".images").slick({arrows : false,});

  function setCSRF () {
    let csrftoken = $("[name=csrfmiddlewaretoken]").val();

    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
          if (!this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken);
          }
      }
    });
  }

  // pop up
  $(document).on("click", ".in", function () {
    $(".pop-up").show();
    $(".login").show();
    $(".registration").hide();
  });

  $(document).on("click", ".overlay-pop-up", function () {
    $(".pop-up").hide();
  });

  $(document).on("click", ".button_registration", function () {
    $(".login").hide();
    $(".registration").show();
  });

  $(document).on("click", "#btn-login", function(e) {
    e.preventDefault();

    setCSRF();

    $.ajax({
      type: "POST",
			url: "/user/sign-in/",
			data: {"username": $("input[name='login']").val(), "password": $("input[name='password']").val()},
			cache: false,
      success: function(data, status) {
        if (data == "OK") {
          window.location.reload(false);
        }
        else {
          $(".login .alert-danger").text(data);
        }
      }
    });
  }); // end btn-login

  $(document).on("click", "#btn-registration", function(e) {
    e.preventDefault();

    setCSRF();

    $(".alert-danger").empty();
    let username = $("input[name='username']").val();
    let email = $("input[name='email']").val();
    let name = $("input[name='first_name']").val();
    let surname = $("input[name='last_name']").val();

    $.ajax({
      type: "POST",
      url: "/user/registration/",
      cache: false,
      data: {"username": username,
             "email": email,
             "first_name": name,
             "last_name": surname,
             "password1": $("input[name='password1']").val(),
             "password2": $("input[name='password2']").val(),
      },
      success: function (data, status) {
        if (data == "OK") {
          $(".registration>form").html("<p class='thanks-for-registration'>Thanks for registration, check your email for verified accountl</p>");
          setTimeout("window.location.reload(false)", 4);
        }
        else {
          let json_data = JSON.parse(data);
          for (let i in json_data) {
            search_div_error = ".registration .error-" + i
            $(search_div_error).text(json_data[i][0]["message"]);
          }
        }
      }
    });

  }); // end btn-registration

  // end pop up

  $(document).on("click", ".delete-favorite", function(e) {
    e.preventDefault();

    setCSRF();

    let text_selector = "#" + this.id;
    console.log(text_selector);
    $.ajax({
      type: "DELETE",
			url: "/api/favorites/" + this.id.replace("favorite-", "")  + "/",
			cache: false,
      success: function(data, statusText, jqXHR) {
        let statusCode = jqXHR.status;
        if (statusCode == 204) {
          console.log(data)
          $(text_selector).parent().remove();
          let count = $(".count-favorites").text();
          $(".count-favorites").text("(" + (parseInt(count.replace("(", "").replace(")", ""), 10) - 1) + ")");
        }
      }
    });
  }); // end button delete-favorite

  $(document).on("click", ".add-to-favorites", function() {

    setCSRF();

    console.log(this.id);
    $.ajax({
      type: "POST",
      cache: false,
      data: {"product": this.id},
      url: "/api/favorites/",
      statusCode: {
        201: function() {
          let count = $(".count-favorites").text();
          $(".count-favorites").text("(" + (parseInt(count.replace("(", "").replace(")", ""), 10) + 1) + ")");
        }
      }
    });
  }); // end button add-favorite

  // Product-detail
  $(document).on("click", "#offers", function(e){
    e.preventDefault();
    $("#reviews").removeClass("reviews-offers-activate");
    $("#offers").addClass("reviews-offers-activate");
    $(".source-review").hide();
    $(".reviews").hide();
    $(".offer").show();

  });

  $(document).on("click", "#reviews", function(e){
    e.preventDefault();
    $("#offers").removeClass("reviews-offers-activate");
    $("#reviews").addClass("reviews-offers-activate");
    $(".source-review").hide();
    $(".offer").hide();
    $(".reviews").show();
  });

  $(document).on("click", ".btn-source-reviews", function(e){
    e.preventDefault();
f
    if ($(this).hasClass("activate")) {
      $(".offer").hide();
      let source_review_selector_text = ".source-review ." + $(this).attr("href");
      $(source_review_selector_text).show();

    }
  });

  $(document).on("click", "#btn-send-review", function(e){
    e.preventDefault();
    setCSRF();

    let product = $("#review-product").attr("value");
    let username = $("#review-username").attr("value");
    let advantages = $("#review-advantages").val();
    let disadvantages = $("#review-disadvantages").val();
    let text = $("#review-text").val();

    let review = $("<div>").attr({class: "review"})

    $.ajax({
      type: "POST",
      cache: false,
      url: $("#send-review").attr("action"),
      data: {"product": product,
             "advantages": advantages,
             "disadvantages": disadvantages,
             "text": text,
            },
      success: function(data, statusText, jqXHR) {
        let statusCode = jqXHR.status;
        if (statusCode == 201) {
          let date = new Date();
          let options = {year: 'numeric',
                         month: 'long',
                         day: 'numeric',
                         hour: 'numeric',
                         minute: 'numeric', }
          $(".reviews>p:first-child").remove();
          $("<p>").text("Name: " + username).appendTo(review);
          $("<p>").text("Advantages: " + advantages).appendTo(review);
          $("<p>").text("Disadvantages: " + disadvantages).appendTo(review);
          $("<p>").text(text).appendTo(review);
          $("<p>").text(date.toLocaleString("en", options)).appendTo(review);
          $("#send-review").before(review);
        }
        else if (statusCode == 200) {
          errors = data["errors"];
          $("#send-review>.errors").empty();
          $("#send-review>.errors").html(errors);
        }
      }
    });
  });

  $(document).on("click", "#django-starfield-rating>input[name=rating]", function(e) {
    e.preventDefault();
    setCSRF();

    let rating = $(this).val();
    let product_id = window.location.pathname.split("/")[2];
    $.ajax({
      type: "POST",
      url: "/api/ratings/",
      data: {"rating": rating, "product": product_id},
      success: function(data, status, jqXHR) {
        let statusCode = jqXHR.status;
        if (statusCode == 201) {
          $("#form-rating").hide();
          let del = parseInt($("#rating").attr("denominator"));
          let prev_average_price = parseFloat($("#rating>span").text());
          let sum_all_rating = (del * prev_average_price) + parseInt(rating);
          let average_price = Math.round((sum_all_rating / (del + 1)) * 100) / 100;
          $("#rating>span").text(average_price);
        }
      }
    });
    // $("#form-rating").submit();
  });
  //End Product-detail
});
