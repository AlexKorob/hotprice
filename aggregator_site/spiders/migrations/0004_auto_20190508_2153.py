# Generated by Django 2.2 on 2019-05-08 18:53

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spiders', '0003_auto_20190508_2151'),
    ]

    operations = [
        migrations.RenameField(
            model_name='spider',
            old_name='minutes',
            new_name='minute',
        ),
    ]
