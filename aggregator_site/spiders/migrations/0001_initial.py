# Generated by Django 2.2 on 2019-05-07 19:19

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Spider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=60, unique=True)),
                ('start_url', models.CharField(max_length=255)),
                ('activate', models.BooleanField(default=False)),
                ('jobid', models.CharField(max_length=60, unique=True)),
                ('schedule', django.contrib.postgres.fields.jsonb.JSONField(default={'day_of_month': 0, 'day_of_week': 0, 'hour': 0, 'minute': 0, 'month_of_year': 0})),
            ],
        ),
    ]
