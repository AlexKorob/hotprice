import json
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, post_delete
from .models import Spider
from django_celery_beat.models import CrontabSchedule, PeriodicTask


@receiver(post_delete, sender=Spider)
@receiver(pre_save, sender=Spider)
def delete_current_periodic(sender, instance=None, **kwargs):
    periodic_task = PeriodicTask.objects.filter(name=instance.name)
    if periodic_task:
        periodic_task.delete()
        instance.enabled = False


@receiver(post_save, sender=Spider)
def create_schedule_spider(sender, update_fields, instance=None, created=False, **kwargs):
    cron_fields = ["minute", "hour", "day_of_week", "day_of_month"]
    spider_name = instance.name
    cron_data = {}
    for attr_name in cron_fields:
        attr = getattr(instance, attr_name)
        if attr != -1:
            cron_data[attr_name] = attr
    if len(cron_data) > 0:
        schedule, _ = CrontabSchedule.objects.get_or_create(**cron_data)
        PeriodicTask.objects.create(crontab=schedule, name=spider_name, task="spiders.tasks.run_spider",
                                    args=json.dumps([spider_name]))
