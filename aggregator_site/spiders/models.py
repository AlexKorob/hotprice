import requests
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator, URLValidator
from django.core.exceptions import ValidationError

from spiders.services import check_spider_on_scrapyd


class Spider(models.Model):
    day_of_week_arr = ["-1", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    day_of_week_choices = [(num, day) for num, day in zip(range(-1, 7), day_of_week_arr)]

    name = models.CharField(max_length=60, blank=False, unique=True)
    start_url = models.CharField(max_length=255, blank=False, validators=(URLValidator(schemes=['http', 'https']), ))
    activate = models.BooleanField(default=False)
    jobid = models.CharField(blank=True, max_length=60)
    minute = models.SmallIntegerField(default=-1, validators=(MinValueValidator(-1), MaxValueValidator(59)))
    hour = models.SmallIntegerField(choices=[(i, str(i)) for i in range(-1, 24)], default=-1,
                                    validators=(MinValueValidator(-1), MaxValueValidator(23)))
    day_of_week = models.SmallIntegerField(choices=day_of_week_choices, default=-1,
                                           validators=(MinValueValidator(-1), MaxValueValidator(6)))
    day_of_month = models.SmallIntegerField(choices=[(i, str(i)) for i in range(-1, 31) if i != 0], default=-1,
                                            validators=(MinValueValidator(-1), MaxValueValidator(31)))

    class Meta:
        ordering = ("-name", )

    def __str__(self):
        text = self.name
        for attr_name in ("minute", "hour", "day_of_week", "day_of_month"):
            attr = getattr(self, attr_name)
            text += f" | {attr_name}: {attr}" if attr != -1 else ""
        return text

    def clean(self):
        error = check_spider_on_scrapyd(self)
        if error:
            raise ValidationError(error)
        elif self.minute == -1 and self.hour == -1 and self.day_of_week == -1 and self.day_of_month == -1:
            raise ValidationError("You must choose the least one of parameneter for run spider")

    @property
    def spider_exist(self):
        spider_name = self.name
        url = "http://localhost:6800/listspiders.json?project=scraping"
        response = requests.get(url).json()
        if (response["status"] == "error") or (spider_name not in response["spiders"]):
            return False
        return True
