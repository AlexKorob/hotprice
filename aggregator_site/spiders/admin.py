from django.contrib import admin
from .models import Spider


@admin.register(Spider)
class SpiderAdmin(admin.ModelAdmin):
    change_list_template = "spiders/add_button.html"
