import requests
import logging
from celery import shared_task
from .services import (create_product, create_source_product, MatchProductName,
                       check_source_product, update_source_product)
from .models import Spider


log = logging.getLogger("file")


@shared_task
def save_products(items):
    for item in items:
        product = MatchProductName(item["name"])
        if not product:
            product = create_product(item)
        source_product = check_source_product(product, item["store"])
        if not source_product:
            create_source_product(item, product)
        else:
            update_source_product(source_product=source_product, product=product, item=item)


@shared_task
def run_spider(spider_name, urls:list=None):  # noqa
    url = "http://localhost:6800/schedule.json"
    data = {"project": "scraping", "spider": spider_name}
    if urls:
        data["urls"] = urls
    response = requests.post(url, data=data).json()
    Spider.objects.filter(name=spider_name).update(jobid=response["jobid"], activate=True)
    log.info(f"Spider - {spider_name} starting...")

    return response["status"]
