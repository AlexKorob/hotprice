import requests

from django_celery_beat.models import PeriodicTask
from rest_framework.response import Response
from rest_framework import status, permissions, generics

from .models import Spider
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action

from .serializers import SpiderSerializer, PeriodicTaskSerializer
from .permissions import IsAdminUser

# class SpiderAPIView(CreateAPIView):
#     def get(self):
#         return


class SpiderViewSet(ModelViewSet):
    """
    create: name -> str
            start_url -> str
            minute -> from 0 to 59
            hour -> from 0 to 23
            day_of_week -> Sunday, Monday, Tuesday...
            day_of_month -> from 0 to 31
    """
    serializer_class = SpiderSerializer
    queryset = Spider.objects.all()
    permission_classes = (IsAdminUser, )

    def perform_update(self, serializer):
        serializer.save()
        instance = serializer.instance
        activate = instance.activate
        urls = instance.start_url
        if activate:
            data = {"project": "scraping", "spider": instance.name, "urls": urls}
            response = requests.post("http://localhost:6800/schedule.json", data=data).json()
            instance.jobid = response["jobid"]
            instance.save()
        elif activate is False:
            jobid = instance.jobid
            data = {"job": jobid, "project": "scraping"}
            requests.post("http://localhost:6800/cancel.json", data=data).json()


class PeriodicTaskUpdateView(generics.UpdateAPIView):
    lookup_field = "name"
    serializer_class = PeriodicTaskSerializer
    queryset = PeriodicTask.objects.all()
    permission_classes = (permissions.IsAdminUser, )
