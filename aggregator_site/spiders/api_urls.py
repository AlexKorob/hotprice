from django.urls import path
from rest_framework.routers import DefaultRouter

from . import api_views

router = DefaultRouter()

router.register(r"", api_views.SpiderViewSet)

urlpatterns = [
    path("periodictask/<str:name>", api_views.PeriodicTaskUpdateView.as_view(), name="periodictask-spiders"),
] + router.urls
