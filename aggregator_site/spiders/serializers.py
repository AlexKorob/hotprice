from rest_framework import serializers
from django_celery_beat.models import PeriodicTask
from django.core.exceptions import ObjectDoesNotExist

from .models import Spider


class SpiderSerializer(serializers.ModelSerializer):
    jobid = serializers.ReadOnlyField()
    enabled = serializers.SerializerMethodField()

    class Meta:
        model = Spider
        fields = ("id", "name", "start_url", "jobid", "activate", "activate", "minute",
                  "hour", "day_of_week", "day_of_month", "spider_exist", "enabled")

    def get_enabled(self, object):
        return PeriodicTask.objects.get(name=object.name).enabled


class PeriodicTaskSerializer(serializers.ModelSerializer):

    class Meta:
        model = PeriodicTask
        fields = ("enabled", )
