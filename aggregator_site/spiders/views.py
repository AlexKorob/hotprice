import requests
import logging
from django.shortcuts import render
from django.views.generic import View
from django.contrib.auth.mixins import PermissionRequiredMixin
from .models import Spider
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
from django_celery_beat.models import PeriodicTask


log_file = logging.getLogger("file")
log_to_email = logging.getLogger("send_on_mail")


class SpidersView(PermissionRequiredMixin, View):
    """
        models.PeriodicTask.name == models.Spider.name == scraping.spiders.some_spider.name
    """
    permission_required = 'is_superuser'

    def get(self, request):
        spiders = Spider.objects.all()
        return render(request, "spiders/spiders_list.html", {"spiders": spiders})

    def post(self, request):
        urls = request.POST.get("urls")
        spider_name = request.POST.get("spider_name")

        action = request.POST.get("action")
        spiders = Spider.objects.all()

        handler = {
            "start": self.start,
            "cancel": self.cancel,
            "disable": self.disable,
            "enable": self.enable,
        }.get(action)

        status, error = handler(urls, spider_name)

        return render(request, "spiders/spiders_list.html", {"spiders": spiders,
                                                             "current_spider": {"name": spider_name,
                                                                                "status": status,
                                                                                "error": error}})

    def disable(self, urls, spider_name):
        periodic_task = PeriodicTask.objects.get(name=spider_name)
        periodic_task.enabled = False
        periodic_task.save()
        log_to_email.warning(f"PeriodicTask of Spider - {spider_name} was disabled")
        return "ok", ""

    def enable(self, urls, spider_name):
        periodic_task = PeriodicTask.objects.get(name=spider_name)
        periodic_task.enabled = True
        periodic_task.save()
        return "ok", ""

    def cancel(self, urls, spider_name):
        jobid = Spider.objects.get(name=spider_name).jobid
        status = "ok"
        if jobid:
            data = {"job": jobid, "project": "scraping"}
            response = requests.post("http://localhost:6800/cancel.json", data=data).json()
            status = response["status"]
            Spider.objects.filter(name=spider_name).update(activate=False)
            log_to_email.warning(f"Spider - {spider_name} with job_id - {jobid} was canceled")
        return status, ""

    def start(self, urls, spider_name):
        is_valid, error = self.urls_validate(urls)
        status = "ok"
        if not error:
            data = {"project": "scraping", "spider": spider_name, "urls": urls}
            response = requests.post("http://localhost:6800/schedule.json", data=data).json()
            Spider.objects.filter(name=spider_name).update(jobid=response["jobid"], activate=True)
            status = response["status"]
            log_to_email.warning(f"Spider - {spider_name} started with: {urls}")
        return status, error

    def urls_validate(self, urls):
        if urls == "":
            return "This field can't be blank"
        clean_urls = []
        for url in [i.strip() for i in urls.split(";") if i.strip() != ""]:
            try:
                URLValidator(schemes=["http", "https"])(url)
                clean_urls.append(url)
            except ValidationError as e:
                log_file.error(e)
                log_to_email.error(e)
                return False, e.message
        return True, ""
