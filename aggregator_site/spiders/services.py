import re
import requests
import boto3
import logging

from django.conf import settings

from products.models import (Product, Image, ReviewSourceProduct,
                             SourceProduct, Store, AveragePrice, Feature)
from categories.models import Category


log = logging.getLogger("file")


key = settings.AWS_SECRET_ACCESS_KEY
key_id = settings.AWS_ACCESS_KEY_ID
bucket_name = settings.AWS_STORAGE_BUCKET_NAME
s3 = boto3.resource("s3", aws_access_key_id=key_id, aws_secret_access_key=key)

regexes = [re.compile(r"\([A-Z\d\.]+[A-Z-]*\d+[A-Z\d\.-]+\)", flags=re.ASCII),
           re.compile(r"\b[A-Z]+[A-Z-]*\d+[A-Z\d-]+\b", flags=re.ASCII),
           re.compile(r"([A-Z0-9]{1,3} ){3,4}?[A-Z0-9]{1,3}", flags=re.ASCII),
           re.compile(r"\(([\d\. -]){5,}\)", flags=re.ASCII),
           re.compile(r"([A-Z]+ ){2,}\d+ [A-Z\+-]{4,}", flags=re.ASCII),
           re.compile(r" [A-Z][A-z]+ ([A-z0-9]+ ){2,}?.+", flags=re.ASCII)]


class MatchCategory(object):
    def __init__(self, categories: list):
        self.categories = categories

    @property
    def category(self):
        exists, category, parent = self.match_category(self.categories[-1], self.categories[-2])

        if exists:
            return category

        elif parent:
            category = Category.objects.create(name=self.categories[-1], parent=parent)
            return category

        elif len(self.categories) >= 2:
            categories_must_be_created = []
            categories_must_be_created.insert(0, self.categories[-1])
            categories_must_be_created.insert(0, self.categories[-2])
            root_category = None
            for categ in reversed(self.categories[:-2]):
                exists, rating, _category = self.match_string_with_category(categ, 40)
                if exists:
                    root_category = _category
                    break
                categories_must_be_created.insert(0, categ)
            if not root_category:
                root_category = Category.objects.create(name=categories_must_be_created.pop(0))
            for category_name in categories_must_be_created:
                root_category = Category.objects.create(name=category_name, parent=root_category)
            category = Category.objects.get(name=categories_must_be_created[-1])
            return category

        else:
            if category:
                category = Category.objects.create(name=category.name)
            else:
                category = Category.objects.create(name=self.categories[-1])
            return category

    def match_category(self, category, parent):
        category_on_db = Category.objects.filter(name__iexact=category)
        if len(category_on_db) == 1:
            # print(f"{category_on_db.first()}: {category} - найдено")

            return (True, category_on_db.first(), None)
        elif len(category_on_db) > 1:
            parent_on_db = Category.objects.filter(name__iexact=parent).first()
            if parent_on_db:
                return (True, Category.objects.filter(name__iexact=category, parent=parent_on_db.id).first(),
                        parent_on_db)
            parent_on_db = self.match_string_with_category(parent, 40)
            if not parent_on_db[0]:
                raise ValueError("Было найдено больше двух категорий, но ни один родилель этих категорий так и \
                                 не был найден в базе", list(category_on_db))
                # parent_on_db = Category.objects.create(name=parent)
                # return Category.objects.create(name=category, parent=parent_on_db)
            category_on_db = Category.objects.filter(name__iexact=category, parent=parent_on_db[-1].id).first()
            # print(f"{category}: {category_on_db} - найдено")
            return (True, category_on_db, parent_on_db[-1])

        elif not category_on_db:
            category_compatible = self.match_string_with_category(category, 70)
            if category_compatible[0]:
                # print(f"{category}: {category_compatible[-1]} - найдено; {category_compatible[1]}/100")
                return (True, category_compatible[-1], None)
            else:
                parent_exist, rating_parent, _parent = self.match_string_with_category(parent, 40)
                current_category = Category.objects.filter(name=category_compatible[-1])

                if len(current_category) > 1:
                    rating_parent = 0
                    for categ in current_category:
                        _parent = Category.objects.get(id=categ.parent_id)
                        compare_parents = self.match_two_strings(parent, _parent.name)
                        if compare_parents > rating_parent:
                            parent_on_db = _parent
                            rating_parent = compare_parents

                if parent_exist:
                    rating_category = 0
                    pretender_category = None
                    for categ in Category.objects.filter(name=_parent.name).get_descendants(include_self=True):
                        rating = self.match_two_strings(category, categ.name)
                        if rating > rating_category:
                            rating_category = rating
                            pretender_category = categ
                    if rating_category > 40:
                        # print(f"{category} - изначально не было найдено - {category_compatible[1]}/100, сопоставление родителя - {parent}; {_parent.name}; - {rating_parent}/100. КАТЕГОРИЯ: {pretender_category}")
                        return (True, pretender_category, _parent)
                    else:
                        # print(f"{category} - не было найденo, родитель категории - {_parent}")
                        return (False, None, _parent)

                else:
                    # print(f"{category} - не найден родитель - {parent}; {_parent} {category_compatible[1]}/100")
                    return (False, None, None)

    def match_string_with_category(self, string, valid_rating):
        array = string.split(" ")

        array_categories_on_db = []
        category_parts_for_search = []
        for part_category_name in array:
            category_parts_for_search.append(part_category_name)
            search = " ".join(category_parts_for_search) if len(array) > 1 else part_category_name
            array_categories_on_db.append(Category.objects.filter(name__icontains=search))

        number_match_categ = float("inf")
        probability_categories = None
        for categories_on_db in array_categories_on_db:
            num = len(categories_on_db)
            if (num < number_match_categ) and (num != 0):
                number_match_categ = num
                probability_categories = categories_on_db

        if probability_categories:
            num_array_match_string_n_symbols = []
            dict_match_rating_n_category_name = {}
            for category in probability_categories:
                rating = self.match_two_strings(string, category.name)
                num_array_match_string_n_symbols.append(rating)
                dict_match_rating_n_category_name[rating] = category
            biggest_rating = max(num_array_match_string_n_symbols)
            if biggest_rating >= valid_rating:
                return [True, biggest_rating, dict_match_rating_n_category_name[biggest_rating]]
            return [False, biggest_rating, dict_match_rating_n_category_name[biggest_rating]]
        return [False, None, None]

    def match_two_strings(self, string_1, string_2):
        bigger_str = string_1.lower() if len(string_1) > len(string_2) else string_2.lower()
        smaller_str = string_2.lower() if len(string_2) < len(string_1) else string_1.lower()
        small_arr = smaller_str.replace(",", "").replace("-", " ").split(" ")
        big_arr = bigger_str.replace(",", "").replace("-", " ").split(" ")
        spaces_on_small_str = smaller_str.count(" ") or smaller_str.count(",") or smaller_str.count("-")
        spaces_on_big_str = bigger_str.count(" ") or bigger_str.count(",") or bigger_str.count("-")  # noqa

        all_num_compatible_symbols = 0

        for word in small_arr:
            partial_word = ""

            num_compatible_symbols = 0

            weight = [n**2 / len(word) for n in range(1, len(word) + 1)]
            for num, symbol in enumerate(word):
                partial_word += symbol
                finds = [word_in_big_arr.find(partial_word) for word_in_big_arr in big_arr]
                if max(finds) == -1:
                    continue
                num_compatible_symbols = weight[num]
            all_num_compatible_symbols += num_compatible_symbols

        return round((all_num_compatible_symbols) * 100 / (len(bigger_str) - spaces_on_small_str), 2)


class MatchProductName(object):
    def __new__(cls, string):
        cls.string = string
        return cls.product()

    @classmethod
    def product(cls):
        gen = cls.find_crucial_sequence()
        try:
            while True:
                identificator = next(gen)
                if identificator:
                    left_slice = identificator.span()[0]
                    right_slice = identificator.span()[1]
                    product = Product.objects.filter(name__contains=cls.string[left_slice:right_slice])
                    if len(product) > 1:
                        print(f"Found with same identificators {cls.string}: {product}")
                    elif len(product) == 1:
                        print(f"{cls.string} match with {product[0].name}")
                        return product.first()
        except StopIteration:
            product = Product.objects.filter(name__icontains=cls.string)
            if not product:
                print(f"{cls.string} not match")
                return None
            elif len(product) == 1:
                return product.first()
            print(f"{cls.string} - Found grade than 1 product")

    @classmethod
    def find_crucial_sequence(cls):
        for regex in regexes:
            yield regex.search(cls.string)


def create_source_product(item, product):
    source_product_price = float(re.sub(r'[^0-9.]+', r'', item["price"]))
    store, created = Store.objects.get_or_create(name=item["store"])
    source_product_data = {"name": item["name"],
                           "price": source_product_price,
                           "rating": item["rating"],
                           "store": store,
                           "url_in_store": item["url_in_store"],
                           "description": item["description"],
                           "product": product}
    source_product = SourceProduct.objects.create(**source_product_data)

    create_average_price(product)

    reviews = item["reviews"]
    if reviews:
        for review in reviews:
            review["source_product"] = source_product
            ReviewSourceProduct.objects.create(**review)
    return source_product


def create_average_price(product):
    all_source_products = SourceProduct.objects.filter(product=product)
    prices_source_products = []
    for source_product in all_source_products:
        prices_source_products.append(float(source_product.price))

    avg_price = round(sum(prices_source_products) / len(prices_source_products), 2)

    if (max(prices_source_products) != min(prices_source_products)) and \
       (avg_price not in prices_source_products):
        average_price, _ = AveragePrice.objects.get_or_create(price=avg_price)
        product.average_price.add(average_price)


def create_product(item):
    category = MatchCategory(item["categories"]).category
    product_data = {"name": item["name"],
                    "category": category,
                    "description": item["description"]}
    product = Product.objects.create(**product_data)

    features = item["features"]
    if features:
        for feature, value in features.items():
            feature, _ = Feature.objects.get_or_create(feature=feature, value=value)
            product.features.add(feature)

    price = re.sub(r'[^0-9.]+', r'', item["price"])
    average_price, _ = AveragePrice.objects.get_or_create(price=price)
    product.average_price.add(average_price)

    folder_for_img = category.name.replace(" ", "_")
    for image in item["images"]:
        img_name = image[8:].replace("/", "_")
        Image.objects.create(product=product, name=img_name)
        response = requests.get(image)
        s3.Object(bucket_name, f"{folder_for_img}/{img_name}").put(Body=response.content)

    Store.objects.get_or_create(name=item["store"])[0]
    return product


def check_source_product(product, store):
    store, _ = Store.objects.get_or_create(name=store)
    return SourceProduct.objects.filter(product=product, store=store).first()


def update_source_product(source_product, product, item):
    price = float(source_product.price)
    rating = float(source_product.rating)
    reviews = ReviewSourceProduct.objects.filter(source_product=source_product)
    url_in_store = source_product.url_in_store
    description = source_product.description

    price_from_site = float(item["price"])
    rating_from_site = item["rating"]
    reviews_from_site = item["reviews"]
    url_from_site = item["url_in_store"]
    description_from_site = item["description"]

    if price != price_from_site:
        source_product.price = price_from_site
        source_product.save()
        create_average_price(product)

    if rating != rating_from_site:
        source_product.rating = rating

    if reviews_from_site:
        if reviews.count() < len(reviews_from_site):
            for review in reviews_from_site:
                review["source_product"] = source_product
                source_product_review, created = ReviewSourceProduct.objects.get_or_create(**review)

    if url_from_site != url_in_store:
        source_product.url_in_store = url_from_site

    if description_from_site != description:
        source_product.description = description_from_site

    source_product.save()


def check_spider_on_scrapyd(instance):
    spider_name = instance.name
    instance.jobid = ""
    url = "http://localhost:6800/listspiders.json?project=scraping"
    response = requests.get(url).json()
    if response["status"] == "error":
        return f"{response['message']}"
    elif response["status"] == "ok":
        if spider_name not in response["spiders"]:
            return f"Spider <{spider_name}> wasn't found in scrapyd"
    return None