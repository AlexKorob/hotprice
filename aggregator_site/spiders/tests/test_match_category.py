from copy import copy
from django.test import TestCase
from spiders.services import MatchCategory, MatchProductName
from categories.models import Category
from products.models import Product


class MatchCategoryTestCase(TestCase):
    fixtures = ("start_category.json", )

    def setUp(self):
        self.count_categories_in_db = Category.objects.all().count()

    def test_match_categories_with_same_categories_from_db(self):
        for category in Category.objects.all():
            if category.parent:
                matched_category = MatchCategory([category.parent.name, category.name]).category
                self.assertEqual(category, matched_category)
                self.assertEqual(category.parent, matched_category.parent)
            else:
                matched_category = MatchCategory([None, category.name]).category
                self.assertEqual(category, matched_category)
        self.assertEqual(Category.objects.all().count(), self.count_categories_in_db)

    def test_match_categories_with_little_diff(self):
        for category in Category.objects.all():
            category_name = category.name.lower()
            category_parent_name = None
            if category.parent:
                category_parent_name = category.parent.name
            length_category_name = len(category_name)
            if length_category_name > 4:
                ended = ["а", "у", "е", "ы", "и"]
                for i in ended:
                    prepare_category_name = category_name[:-1]
                    prepare_category_name + i
                    category = MatchCategory([category_parent_name, prepare_category_name]).category
                    self.assertEqual(category.name.lower(), category_name)
            if length_category_name > 8:
                ended = ["ки", "ку", "ка", "си", "ди", "ее", "не", "га", "кр"]
                for i in ended:
                    prepare_category_name = category_name[:-1]
                    prepare_category_name + i
                    category = MatchCategory([category_parent_name, prepare_category_name]).category
                    self.assertEqual(category.name.lower(), category_name)
        self.assertEqual(Category.objects.all().count(), self.count_categories_in_db)

    def test_match_category_with_not_exist_category_in_db_but_parent_is_exit(self):
        categories = ["Комплектующие", "Some_does_not_exit_before_category"]
        category = MatchCategory(categories).category
        self.assertEqual(category.name, categories[-1])
        self.assertEqual(Category.objects.all().count(), self.count_categories_in_db + 1)
        self.assertTrue(Category.objects.get(name=categories[-1]))
        self.assertEqual(category.parent.name, categories[-2])

    def test_math_category_with_not_exist_category_in_db_and_not_exist_parent_too(self):
        categories = ["Компьютерная техника", "Some_does_not_exit_parent_by_current_category", "Some_does_not_exit_before_category"]
        category = MatchCategory(categories).category
        self.assertEqual(category.name, categories[-1])
        self.assertEqual(Category.objects.all().count(), self.count_categories_in_db + 2)
        self.assertTrue(Category.objects.get(name=categories[-1]))
        self.assertTrue(Category.objects.get(name=categories[-2]))
        self.assertEqual(category.parent.name, categories[-2])
