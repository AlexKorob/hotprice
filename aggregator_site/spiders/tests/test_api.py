from unittest.mock import patch, PropertyMock, Mock

from django.shortcuts import reverse

from rest_framework.test import APITestCase

from aggregator_site.base_tests import BaseTestAPI
from spiders.models import Spider


class CreateSpiderTestCase(APITestCase, BaseTestAPI):
    data = {"name": "example",
            "start_url": "http://example.com",
            "activate": True,
            "minute": 0,
            "hour": "0",
            "day_of_week": "1",
            "day_of_month": "1"
            }

    @patch("spiders.models.Spider.spider_exist", new_callable=PropertyMock, return_value=True)
    def test_admin_create_spider(self, spider_exist_attr):
        self.auth("admin")
        spiders_count = Spider.objects.count()
        response = self.client.post(reverse("spider-list"), data=self.data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Spider.objects.count(), spiders_count + 1)

    def test_moderator_create_spider(self):
        self.auth("moderator")
        spiders_count = Spider.objects.count()
        response = self.client.post(reverse("spider-list"), data=self.data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Spider.objects.count(), spiders_count)

    def test_simple_user_create_spider(self):
        self.auth("user")
        spiders_count = Spider.objects.count()
        response = self.client.post(reverse("spider-list"), data=self.data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Spider.objects.count(), spiders_count)


class SpiderUpdateTestCase(APITestCase, BaseTestAPI):
    fixtures = ("spiders", )
    pk = Spider.objects.first().pk

    data = {"name": "example",
            "start_url": "http://example.com",
            "activate": True,
            "minute": 0,
            "hour": "0",
            "day_of_week": "1",
            "day_of_month": "1",
            }

    @patch("spiders.models.Spider.spider_exist", new_callable=PropertyMock, return_value=True)
    @patch("spiders.api_views.requests")
    def test_admin_update_spider(self, mock_requests, spider_exist):
        self.auth("admin")
        mock_requests.post.return_value = Mock(
            json=(Mock(return_value={"jobid": "jobid"}))
        )

        response = self.client.put(reverse("spider-detail", kwargs={"pk": self.pk}),
                                   data=self.data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["name"], Spider.objects.get(name=self.data["name"]).name)

        response = self.client.patch(reverse("spider-detail", kwargs={"pk": self.pk}),
                                     data={"name": "some_name"})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["name"], Spider.objects.get(name="some_name").name)

    def test_simple_user_update_spider(self):
        self.auth("user")
        response = self.client.put(reverse("spider-detail", kwargs={"pk": self.pk}),
                                   data=self.data)
        self.assertEqual(response.status_code, 403)

        response = self.client.patch(reverse("spider-detail", kwargs={"pk": self.pk}),
                                     data={"name": "some_name"})
        self.assertEqual(response.status_code, 403)

    def test_moderator_user_update_spider(self):
        self.auth("moderator")
        response = self.client.put(reverse("spider-detail", kwargs={"pk": self.pk}),
                                   data=self.data)
        self.assertEqual(response.status_code, 403)

        response = self.client.patch(reverse("spider-detail", kwargs={"pk": self.pk}),
                                     data={"name": "some_name"})
        self.assertEqual(response.status_code, 403)


class SpiderDeleteTestCase(APITestCase, BaseTestAPI):
    fixtures = ("spiders.json", )
    pk = Spider.objects.first().pk

    def test_admin_delete_spider(self):
        self.auth("admin")
        num_spiders = Spider.objects.count()
        response = self.client.delete(reverse("spider-detail", kwargs={"pk": self.pk}))

        self.assertEqual(response.status_code, 204)
        self.assertEqual(Spider.objects.count(), num_spiders - 1)

    def test_simple_user_delete_spider(self):
        self.auth("user")
        num_spiders = Spider.objects.count()
        response = self.client.delete(reverse("spider-detail", kwargs={"pk": self.pk}))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Spider.objects.count(), num_spiders)

    def test_moderator_user_delete_spider(self):
        self.auth("moderator")
        num_spiders = Spider.objects.count()
        response = self.client.delete(reverse("spider-detail", kwargs={"pk": self.pk}))
        self.assertEqual(response.status_code, 403)
        self.assertEqual(Spider.objects.count(), num_spiders)

