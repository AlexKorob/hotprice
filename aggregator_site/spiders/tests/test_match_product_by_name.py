import random
from string import ascii_letters
from django.test import TestCase
from products.models import Product
from spiders.services import MatchProductName


class MatchProductByNameTestCase(TestCase):
    fixtures = ("start_category.json", "products.json")

    def test_match_products_with_same_products_in_db(self):
        for product in Product.objects.all():
            matched_product = MatchProductName(product.name)
            self.assertEqual(matched_product, product)

    def test_match_products_with_little_diff(self):
        alphabet = list(ascii_letters)
        for product in Product.objects.all():
            product_name_list = product.name.split(" ")
            if len(product_name_list) > 1:
                first_name_on_product_name = "".join([random.choice(alphabet) for i in range(random.randrange(1, 10))])
                product_name_list.pop(0)
                product_name_list.insert(0, first_name_on_product_name)
                product_name = " ".join(product_name_list)
            else:
                continue
            matched_product = MatchProductName(product_name)
            self.assertEqual(matched_product, product)

    def test_match_product_with_not_exist_product_on_db(self):
        product_name = "Not_exist_product"
        product = MatchProductName(product_name)
        self.assertEqual(product, None)
