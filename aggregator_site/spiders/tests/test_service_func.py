from django.test import TestCase
from django.db.models import Avg
from unittest.mock import Mock, patch, PropertyMock
from products.models import Product, SourceProduct, AveragePrice
from categories.models import Category
from aggregator_site.base_tests import BaseTest
from spiders.services import create_source_product, create_average_price, create_product


class UtilsTestCase(TestCase, BaseTest):

    def test_create_source_product(self):
        cnt_source_products = SourceProduct.objects.all().count()
        product = self.create_product()
        self.assertTrue(product)
        source_product_data = {"name": "auto 2121",
                               "price": "12000",
                               "rating": "5",
                               "store": "АвтоВаз",
                               "url_in_store": "https://autovaz.ru",
                               "description": "Not",
                               "product": product,
                               "reviews": ""}
        source_product = create_source_product(source_product_data, product)
        self.assertTrue(source_product)
        self.assertEqual(SourceProduct.objects.all().count(), cnt_source_products + 1)

    def test_create_source_product_wit_empty_data(self):
        product = self.create_product()
        with self.assertRaises(KeyError) as error:
            source_product_data = {}
            source_product = create_source_product(source_product_data, product)

    @patch("spiders.services.SourceProduct.objects.filter")
    def test_create_avg_price(self, source_products_mock):
        product = self.create_product()
        source_products = []
        prices = [12.00, 45.00, 23.00]
        for price in prices:
            source_products.append(Mock(price=price))
        avg_price = round(sum(prices) / len(prices), 2)
        source_products_mock.return_value = source_products
        create_average_price(product)
        self.assertEqual(AveragePrice.objects.all().count(), 1)
        self.assertEqual(float(Product.objects.get(name=product.name).average_price.first().price), avg_price)

    @patch("spiders.services.MatchCategory.category", new_callable=PropertyMock)
    @patch("spiders.services.requests.get")
    @patch("spiders.services.s3")
    def test_create_product(self, mock_s3, mock_request_get, mock_match_category):
        mock_s3.return_value.put.return_value = None
        mock_request_get.return_value.content = None
        category = Category.objects.create(name="category")
        mock_match_category.return_value = category
        product_data = {"name": "Name",
                        "description": "None",
                        "price": "100.00",
                        "features": "",
                        "images": ["http://img.png", "http://img_1.png", "http://img_2.png"],
                        "store": "Store",
                        "categories": None}
        product = create_product(product_data)
        self.assertEqual(Product.objects.all().count(), 1)
        self.assertEqual(product.name, Product.objects.get(name=product.name).name)

    def test_create_product_without_data(self):
        with self.assertRaises(KeyError):
            create_product({})
