from django.test import TestCase
from unittest.mock import patch
from spiders.models import Spider
from aggregator_site.base_tests import BaseTest
from django.urls import reverse
from django_celery_beat.models import PeriodicTask
from unittest.mock import MagicMock, Mock


class SpiderViewTestCase(TestCase, BaseTest):
    """
        first spider activated in fixtures, another spiders - activate == False
    """
    fixtures = ("spiders.json", )

    def setUp(self):
        # auth provided the superuser by default
        self.auth()

    def test_show_all_spiders_by_superuser(self):
        spiders = Spider.objects.all()
        response = self.client.get(reverse("spiders:spiders"))
        self.assertEqual(response.status_code, 200)
        self.assertIn(spiders[0].name, response.content.decode("utf-8"))
        self.assertIn(spiders[1].name, response.content.decode("utf-8"))

    def test_show_all_spiders_by_simple_user(self):
        self.auth("Alisa")  # simple user
        response = self.client.get(reverse("spiders:spiders"))
        self.assertEqual(response.status_code, 403)

    def test_turn_on_some_spider(self):
        spider = Spider.objects.get(id=1)
        response = self.client.post(reverse("spiders:spiders"), data={"action": "enable", "spider_name": spider.name})
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context["current_spider"]["status"] == "ok")
        self.assertEqual(response.context["current_spider"]["name"], spider.name)
        self.assertEqual(PeriodicTask.objects.get(name=spider.name).enabled, True)

    def test_turn_off_some_spider(self):
        spider = Spider.objects.get(id=1)
        # activate spider task
        response = self.client.post(reverse("spiders:spiders"), data={"action": "enable", "spider_name": spider.name})
        self.assertEqual(response.status_code, 200)
        # deactivate spider task
        response = self.client.post(reverse("spiders:spiders"), data={"action": "disable", "spider_name": spider.name})
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context["current_spider"]["status"] == "ok")
        self.assertEqual(response.context["current_spider"]["name"], spider.name)
        self.assertEqual(PeriodicTask.objects.get(name=spider.name).enabled, False)

    @patch("spiders.views.requests.post")
    def test_cancel_or_pause_spider(self, mock_request_post_to_scrapyd):
        mock_request_post_to_scrapyd.return_value.json.return_value = {"status": "ok"}
        spider = Spider.objects.get(id=1)
        self.assertEqual(spider.activate, True)
        response = self.client.post(reverse("spiders:spiders"), data={"action": "cancel", "spider_name": spider.name})
        self.assertEqual(response.status_code, 200)
        spider = Spider.objects.get(id=1)
        self.assertEqual(spider.activate, False)

    @patch("spiders.views.SpidersView.urls_validate", return_value=(True, ""))
    @patch("spiders.views.requests.post")
    def test_start_spider(self, mock_request_post_to_scrapyd, mock_urls_validate):
        jobid = "just_job_id"
        spider = Spider.objects.get(id=2)
        self.assertEqual(spider.activate, False)
        mock_request_post_to_scrapyd.return_value.json.return_value = {"status": "ok", "jobid": jobid}
        response = self.client.post(reverse("spiders:spiders"), data={"action": "start", "spider_name": spider.name})
        self.assertEqual(response.status_code, 200)
        spider = Spider.objects.get(id=2)
        self.assertEqual(spider.activate, True)
        self.assertEqual(spider.jobid, jobid)
