from django.apps import AppConfig


class SpidersConfig(AppConfig):
    name = 'spiders'

    def ready(self):
        import spiders.signals
