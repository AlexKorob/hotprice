from django.urls import path

from rest_framework.routers import DefaultRouter

from . import views

app_name = "spiders"

urlpatterns = [
    path("", views.SpidersView.as_view(), name="spiders"),
]
