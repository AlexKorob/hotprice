from django.contrib.auth import get_user_model
from django.urls import reverse
from django.contrib.auth.models import Group

from rest_framework.authtoken.models import Token

from products.models import Product, ReviewProduct
from categories.models import Category


class BaseTest:
    """
        Default auth() user is first in property - "usernames" and he is_superuser == True,
        all another users is_superuser == False
    """
    usernames = ("alex", "katya", "oleg")
    _created_users = []
    _create_default_users = False

    def get_user_data(self, username):
        is_superuser = False
        if username == self.usernames[0]:
            is_superuser = True
        return {"username": f"{username}",
                "email": f"{username}@gmail.com",
                "first_name": f"{username.title()}",
                "last_name": "last_name",
                "password": "123",
                "is_superuser": is_superuser}

    def create_users(self):
        for user in self.usernames:
            # NOTE "create_user" - this is neccessary, because this method set a password-hash from string
            get_user_model().objects.create_user(**self.get_user_data(username=user))
        self._created_users.extend(self.usernames)
        self._create_default_users = True

    def create_user_by_username(self, username):
        get_user_model().objects.create_user(**self.get_user_data(username=username))
        self._created_users.append(username)

    def auth(self, username=usernames[0]):
        if not self._create_default_users:
            self.create_users()
        if username not in self._created_users:
            self.create_user_by_username(username)
        self.user = get_user_model().objects.get(username=username)
        self.client.login(username=username, password="123")

    def logout(self):
        self.client.logout()

    def create_product(self, data=None):
        # avg_price = AveragePrice.objects.create(price=)
        if not data:
            category = Category.objects.create(name="Автомобили")
            data = {"name": "vaz 2121", "category": category, "description": "None"}
            # "average_price":
        return Product.objects.create(**data)


class BaseTestAPI(object):
    admin = None
    simple_user = None
    moderator = None
    products_id = (1, 2, 3)

    def create_moderator(self):
        self.moderator = get_user_model().objects.create_user(**self.get_data("Oleg", False))
        group = self.get_group("Moderators")
        self.moderator.groups.add(group)

    def create_admin(self):
        self.admin = get_user_model().objects.create_user(**self.get_data("alex", True))

    def create_simple_user(self):
        self.simple_user = get_user_model().objects.create_user(**self.get_data("Veronika", False))

    def get_admin(self):
        if not self.admin:
            self.create_admin()
        return self.admin

    def get_data(self, username, is_superuser=False):
        return {
                "username": f"{username}",
                "password": "123",
                "email": f"{username}@gmail.com",
                "first_name": f"{username.title()}",
                "last_name": f"{username}",
                "is_superuser": is_superuser,
                }

    def get_user(self):
        if not self.simple_user:
            self.create_simple_user()
        return self.simple_user

    def auth(self, user="admin"):
        if user == "admin":
            token = Token.objects.get(user=self.get_admin())
        elif user == "user":
            token = Token.objects.get(user=self.get_user())
        elif user == "moderator":
            token = Token.objects.get(user=self.get_moderator())
        self.client.credentials()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token.key)

    def get_moderator(self):
        if not self.moderator:
            self.create_moderator()
        return self.moderator

    def get_group(self, name="Moderators"):
        if Group.objects.filter(name=name).exists():
            return Group.objects.get(name=name)
        return Group.objects.create(name=name)

    def logout(self):
        self.client.credentials()

    def create_favorites(self):
        for i in self.products_id:
            self.client.post(reverse("favorite-list"), data={"product": i})

    def create_review(self, product_id=1):
        ReviewProduct.objects.create(**{"pk": 1,
                                        "user": self.get_user(),
                                        "product_id": product_id,
                                        "text": "good item",
                                        "advantages": "good",
                                        "disadvantages": "none"
                                        }
                                     )
