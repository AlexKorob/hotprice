import logging.config


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(message)s',
            'datefmt': '%H:%M:%S %Y-%m-%d',
        },
        'simple': {
            'format': '%(levelname)s %(message)s',
            'datefmt': '%H:%M:%S %Y-%m-%d',
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'filters': ['require_debug_true'],
            'formatter': 'verbose',
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filters': ['require_debug_true'],
            'filename': 'logs.txt',
            'mode': 'a',
            'formatter': 'verbose',
        },
        'consoleError': {
            'level': 'ERROR',
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    'loggers': {
        'null': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'file': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
        'django': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        'console': {
            'handlers': ['console'],
            'level': 'INFO'
        }
    }
}

logging.config.dictConfig(LOGGING)
