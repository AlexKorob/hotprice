from rest_framework import serializers


class RecursiveField(serializers.Serializer):
    def __init__(self, field, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.field = field

    def to_representation(self, value):
        serializer = getattr(self.parent, self.field).__class__(value, context=self.context)
        return serializer.data