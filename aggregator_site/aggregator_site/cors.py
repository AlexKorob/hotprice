from django.utils.deprecation import MiddlewareMixin

from .local_settings import ALLOWED_HOSTS


class CorsMiddleware(MiddlewareMixin):

    def process_response(self, req, resp):
        origin = req.headers["Origin"] if req.headers.get("Origin", "").replace("https://", "").replace("http://", "")\
                                          in ALLOWED_HOSTS else ""
        resp["Access-Control-Allow-Origin"] = origin
        return resp