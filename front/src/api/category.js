import axios from 'axios';

export default {
    categories() {
        return axios.get('/localhost:8000/api/categories/');
    }
}