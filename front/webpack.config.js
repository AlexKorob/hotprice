let path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin'); // плагин для загрузки кода Vue
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

module.exports = {
    entry: './src/main.js',
    mode: "development",
    output: {
       path: path.resolve(__dirname, './dist'),
       publicPath: '/dist/',
       filename: 'build.js'
     },
     module: {
       rules: [
         {
           test: /\.vue$/,
           loader: 'vue-loader'
         }, {
          test: /\.css$/,
          use: [
            'vue-style-loader',
            'css-loader'
          ]
        }
       ],
      },
     plugins: [
        new VueLoaderPlugin()
    ]
};