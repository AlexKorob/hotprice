## HotPrice

#### Prepare

```bash
  git clone git@gitlab.com:AlexKorob/hotprice.git
  cd aggregator
  python3 -m venv ./venv
  . venv/bin/activate
  pip3 install -r requirements.txt
```

#### Configure Postgresql:

```bash
  sudo su - postgres
  psql
  CREATE DATABASE hotprice;
  CREATE USER alex WITH PASSWORD '123';
  GRANT ALL PRIVILEGES ON DATABASE hotprice TO alex;
  \q
  logout
```

#### Start Celery
** path: ..../aggregator/aggregator_site$

```bash
  celery -A spiders worker -B -l info --purge --concurrency=2
```

#### Scrapyd
** path: ..../aggregator/scraping$

```bash
  scrapyd
```

#### Deploy spiders to scrapyd
** path ..../aggregator/scraping$

```bash
  scrapyd-deploy local -p scraping
```

#### Run
** path: ..../aggregator/aggregator_site$

```bash
  ./manage.py migrate
  /home/alexandr/Desktop/light_IT/aggregator/venv/bin/gunicorn -w 1 --bind unix:/home/alexandr/Desktop/light_IT/aggregator/aggregator_site/aggregator_site.sock aggregator_site.wsgi:application
```
** You need also create Group with name - "Moderators" and add corresponding permissions

#### Swagger

http://{host}:{port}/swagger/
